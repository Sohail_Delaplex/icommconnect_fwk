//
//  Created by Ravikanth on 8/19/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//
/* == Colors == */
#define kColorsDarkBlack [NSArray arrayWithObjects: \
(id)[[UIColor colorWithRed:.1f green:.1f blue:.1f alpha:0.7] CGColor], \
(id)[[UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.7] CGColor], \
nil]
#define kColorsBlue [NSArray arrayWithObjects: \
(id)[[UIColor colorWithRed:.0f green:.0f blue:.5f alpha:0.7] CGColor], \
(id)[[UIColor colorWithRed:0.f green:0.f blue:1.f alpha:0.7] CGColor], \
nil]
#define kCallTimerSuicide	1.5f
#define headingFont [UIFont boldSystemFontOfSize:18]
#define titleFont [UIFont boldSystemFontOfSize:17]
#define subTitleFont [UIFont boldSystemFontOfSize:16]
#define detailedFont [UIFont boldSystemFontOfSize:15]
#define kScreenWidth [[UIScreen mainScreen] applicationFrame].size.width
//******** Tiles Dimentions **********
static float boxWidth=80;//Tile width
static float boxHeight=80;
//*********Sservices paths ****************
//#define kBaseURL @"https://client.icommconnect.com" //Prod
//#define kBaseURL @"https://client-uat.icommconnect.com" //UAT
#define kBaseURL @"http://192.168.1.134/ICommConnect" //local

//********* Messages ****************
static NSString *IPBlocked_Message=@"Sorry, Call cannot be completed.";
static NSString *LinesBusy_Message=@"Lines are busy, please stay in line to connect ..!";

static NSString *Delinquent_Message=@"Invalid Subscription. Please contact";

static int apiCallIntervel=10;// api call intervel to check whether active lines are ready to connect user.
