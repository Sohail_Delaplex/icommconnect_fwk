//
//  ICommConnectAppDelegate.h
//  ICommConnect
//
//  Created by Ravikanth on 9/4/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AudioCallViewController.h"
#import "SubScriptionViewController.h"
#import "LandingPageViewController.h"
@interface ICommConnectAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
@private
    AudioCallViewController *audioCallController;
    SubScriptionViewController *subScriptionViewController;
    LandingPageViewController *landingPageViewController;
    NgnEngine* mEngine;
    BOOL multitaskingSupported;
    NSString *contactGUId;
}
//@property (nonatomic, retain) NSString *subscription_ID;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) AudioCallViewController *audioCallController;
@property (nonatomic, retain) SubScriptionViewController *subScriptionViewController;
@property (nonatomic, retain) LandingPageViewController *landingPageViewController;
+(ICommConnectAppDelegate*) sharedInstance;
-(void)clearDefaults;
@end
