//
//  ICommConnectAppDelegate.m
//  ICommConnect
//
//  Created by Ravikanth on 9/4/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "ICommConnectAppDelegate.h"
#import "ContactViewController.h"
#undef TAG
#define kTAG @"ICommConnectAppDelegate///: "
#define TAG kTAG
@implementation ICommConnectAppDelegate
@synthesize window;
@synthesize audioCallController;
@synthesize subScriptionViewController;
@synthesize landingPageViewController;
//@synthesize subscription_ID;
static UIBackgroundTaskIdentifier sBackgroundTask = UIBackgroundTaskInvalid;
static dispatch_block_t sExpirationHandler = nil;
- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  //  [self setUserDefaults];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    [self.window setFrame:bounds];
    [self.window setBounds:bounds];
    return YES;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
   NSArray *components=[[url query] componentsSeparatedByString:@"="];
    
    if ([components[0] isEqualToString:@"SubscriptionGUId"]&&![components[0] isKindOfClass:[NSNull class]]) {
        [self clearDefaults];
        [self setObjectInDefaults:components[1] withKey:@"SubscriptionGUId"];
       // [self.subScriptionViewController refreshSettings];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        SubScriptionViewController *pushToController = (SubScriptionViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"SubscriptionView"];
         UINavigationController *navigationController = [[[UINavigationController alloc] initWithRootViewController:pushToController] autorelease];
        window.rootViewController = navigationController;
        [window makeKeyAndVisible];
        [self setObjectInDefaults:@"Yes" withKey:@"RefreshApp"];
    }else if([components[0] isEqualToString:@"ContactGUId"]&&![components[0] isKindOfClass:[NSNull class]]) {
        [self clearDefaults];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ContactViewController *pushToController = (ContactViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactView"];
        pushToController.contactGuid=components[1];
        UINavigationController *navigationController = [[[UINavigationController alloc] initWithRootViewController:pushToController] autorelease];
        window.rootViewController = navigationController;
        [window makeKeyAndVisible];
        [self setObjectInDefaults:@"Yes" withKey:@"RefreshApp"];
    }
    return YES;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self clearDefaults];
    UINavigationController *navigationController = [[[UINavigationController alloc] initWithRootViewController:self.landingPageViewController] autorelease];
    window.rootViewController = navigationController;
    [window makeKeyAndVisible];
    	multitaskingSupported = [[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported];
    sBackgroundTask = UIBackgroundTaskInvalid;
    sExpirationHandler = ^{
        NgnNSLog(TAG, @"Background task completed");
        // keep awake
        if([[NgnEngine sharedInstance].sipService isRegistered]){
            if([[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_KEEPAWAKE]){
                [[NgnEngine sharedInstance] startKeepAwake];
            }
        }
        [[UIApplication sharedApplication] endBackgroundTask:sBackgroundTask];
        sBackgroundTask = UIBackgroundTaskInvalid;
    };
    
    if(multitaskingSupported){
        NgnNSLog(TAG, @"Multitasking IS supported");
    }
    [self getAppVersion];
    return YES;
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // application.idleTimerDisabled = YES;
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
    if([ICommConnectAppDelegate sharedInstance]->multitaskingSupported){
        ConnectionState_t registrationState = [[NgnEngine sharedInstance].sipService getRegistrationState];
        if(registrationState == CONN_STATE_CONNECTING || registrationState == CONN_STATE_CONNECTED){
            NgnNSLog(TAG, @"applicationDidEnterBackground (Registered or Registering)");
            //if(registrationState == CONN_STATE_CONNECTING){
            // request for 10min to complete the work (registration, computation ...)
            sBackgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:sExpirationHandler];
            //}
            if(registrationState == CONN_STATE_CONNECTED){
                if([[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_KEEPAWAKE]){
                    if(![NgnAVSession hasActiveSession]){
                        [[NgnEngine sharedInstance] startKeepAwake];
                    }
                }
            }
            
            [application setKeepAliveTimeout:600 handler: ^{
                NgnNSLog(TAG, @"applicationDidEnterBackground:: setKeepAliveTimeout:handler^");
            }];
        }
    }
#endif /* __IPHONE_OS_VERSION_MIN_REQUIRED */
}
- (void)applicationWillEnterForeground:(UIApplication *)application{
    ConnectionState_t registrationState = [[NgnEngine sharedInstance].sipService getRegistrationState];
    NgnNSLog(TAG, @"applicationWillEnterForeground and RegistrationState=%d, NetworkReachable=%s", registrationState, [NgnEngine sharedInstance].networkService.reachable ? "TRUE" : "FALSE");
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
    // terminate background task
    if(sBackgroundTask != UIBackgroundTaskInvalid){
        [[UIApplication sharedApplication] endBackgroundTask:sBackgroundTask]; // Using shared instance will crash the application
        sBackgroundTask = UIBackgroundTaskInvalid;
    }
    // stop keepAwake
    [[NgnEngine sharedInstance] stopKeepAwake];
    
#endif /* __IPHONE_OS_VERSION_MIN_REQUIRED */
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshApp"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RefreshApp"];
//        UINavigationController *navigationController = [[[UINavigationController alloc] initWithRootViewController:self.subScriptionViewController] autorelease];
//        window.rootViewController = navigationController;
//        [window makeKeyAndVisible];
    }

}
- (void)applicationWillTerminate:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    mEngine=[NgnEngine sharedInstance];
    [mEngine stop];
    [mEngine release];
    
}
-(SubScriptionViewController *)subScriptionViewController{
    if(!self->subScriptionViewController){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        self->subScriptionViewController = (SubScriptionViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"SubscriptionView"];
    }
    return self->subScriptionViewController;
}
-(LandingPageViewController *)landingPageViewController{
    if(!self->landingPageViewController){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        self->landingPageViewController = (LandingPageViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    }
    return self->landingPageViewController;
}
+(ICommConnectAppDelegate*) sharedInstance{
    return ((ICommConnectAppDelegate*) [[UIApplication sharedApplication] delegate]);
}
- (void) clearDefaults {
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
//-(void)setUserDefaults{
//    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"SubscriptionGUId"]) {
//        [self setSubscriptionGUIdInDefaults:subscriptionID withKey:@"SubscriptionGUId"];
//    }
//}
-(void)setObjectInDefaults:(NSString*)object withKey:(NSString*)key{
 [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
}
- (void)dealloc {
    [audioCallController release];
    [subScriptionViewController release];
    [window release];
    [super dealloc];
}
-(void)getAppVersion{
    NSString *appVersion=[NSString stringWithFormat:@"iOSApp_version %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    NSLog(@"appVersion:%@",appVersion);
    [self setObjectInDefaults:appVersion withKey:@"AppVersion"];
}
@end
