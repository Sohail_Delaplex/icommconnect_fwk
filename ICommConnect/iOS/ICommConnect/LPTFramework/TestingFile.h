//
//  TestingFile.h
//  LPTFramework
//
//  Created by mac on 6/1/21.
//  Copyright © 2021 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iOSNgnStack.h"

@interface TestingFile : NSObject
{

    NgnAVSession* audioSession;
    BOOL numpadIsVisible;
    CGFloat bottomButtonsPadding;
    NgnEngine* mEngine;
    NgnBaseService<INgnSipService>* mSipService;
    NgnBaseService<INgnConfigurationService>* mConfigurationService;
    BOOL mScheduleRegistration;
}

-(void)startNGNEngine;

@end
