//
//  TestingFile.m
//  LPTFramework
//
//  Created by mac on 6/1/21.
//  Copyright © 2021 Doubango Telecom. All rights reserved.
//

#import "TestingFile.h"

@implementation TestingFile


//registering the sip details of subscriber
-(void)startNGNEngine{

    // add observers
    // take an instance of the engine
    [NgnEngine initialize];
    mEngine = [NgnEngine sharedInstance];
    
    // take needed services from the engine
    mSipService = mEngine.sipService;
    mConfigurationService = mEngine.configurationService;
    
    // start the engine
//    [mEngine start];
//    NSArray *proxyArray=[self splitTheString:[sipDictionary objectForKey:@"SIPUDP"]];
//    // set credentials
//    NSString *publicIdentity=[sipDictionary objectForKey:@"PublicIdentity"];
//    [mConfigurationService setStringWithKey: IDENTITY_IMPI andValue: [sipDictionary objectForKey:@"PrivateIdentity"]];
//    [mConfigurationService setStringWithKey: IDENTITY_IMPU andValue: publicIdentity];
//    [mConfigurationService setStringWithKey: IDENTITY_PASSWORD andValue: [sipDictionary objectForKey:@"Password"]];
//
//    [mConfigurationService setStringWithKey: NETWORK_REALM andValue: [[publicIdentity componentsSeparatedByString:@"@"] objectAtIndex:1]];
//    [mConfigurationService setStringWithKey: NETWORK_PCSCF_HOST andValue:proxyArray[0]];
//     [mConfigurationService setIntWithKey: NETWORK_PCSCF_PORT andValue: [proxyArray[1] intValue]];
////    [mConfigurationService setStringWithKey: NETWORK_PCSCF_HOST andValue:@"192.168.1.9"];
////    [mConfigurationService setIntWithKey: NETWORK_PCSCF_PORT andValue:5070];
//    [mConfigurationService setBoolWithKey: NETWORK_USE_EARLY_IMS andValue: kEnableEarlyIMS];
//    [mConfigurationService setStringWithKey: IDENTITY_DISPLAY_NAME andValue: [sipDictionary objectForKey:@"DisplayName"]];
//    // Override point for customization after application launch
    
    // Try to register the default identity
    [mSipService registerIdentity];
}

@end
