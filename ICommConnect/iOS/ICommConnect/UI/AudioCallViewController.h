//
//  AudioCallViewController.h
//  ICommConnect
//
//  Created by Ravikanth on 9/3/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "BaseViewController.h"
#import "iOSNgnStack.h"
#import "SipDelegate.h"
#import "GetActiveCallsCountDelegate.h"
#import "GetMyIPDelegate.h"
#import "CanAllowCall.h"
@interface AudioCallViewController : BaseViewController<GetActiveCallsCountDelegate,SipDelegate,GetMyIPDelegate,CanAllowCall,UIAlertViewDelegate>
{
	UIView *viewCenter;
	UIView *viewBottom;
	UIButton *buttonHangup;
	UIButton *buttonMute;
	UIButton *buttonNumpad;
	UIImageView *contactImageView;
	UIView *viewNumpad;
    UILabel *callStatus;
    UIButton *buttonSpeaker;
	
	NgnAVSession* audioSession;
	BOOL numpadIsVisible;
	CGFloat bottomButtonsPadding;
    NgnEngine* mEngine;
    NgnBaseService<INgnSipService>* mSipService;
    NgnBaseService<INgnConfigurationService>* mConfigurationService;
    BOOL mScheduleRegistration;
}
@property (retain, nonatomic)NSString *CallToTheNumber;
@property (retain, nonatomic)NSString *contactPersonName;
@property (retain, nonatomic) NSDictionary *contactDictionary;
@property (nonatomic) int contactIndex;
@property (retain, nonatomic)NSData *imageData;
@property (nonatomic) long sessionId;
@property (retain, nonatomic) IBOutlet UIView *viewCenter;
@property (retain, nonatomic) IBOutlet UIView *viewBottom;
@property (retain, nonatomic) IBOutlet UIButton *buttonHangup;
@property (retain, nonatomic) IBOutlet UIButton *buttonMute;
@property (retain, nonatomic) IBOutlet UIButton *buttonNumpad;
@property (retain, nonatomic) IBOutlet UIButton *buttonSpeaker;
@property (retain, nonatomic) IBOutlet UIView *profileImageView;
@property (retain, nonatomic) IBOutlet UIImageView *contactImageView;
@property (retain, nonatomic) IBOutlet UIView *viewNumpad;
@property (retain, nonatomic) IBOutlet UILabel *contactName;
@property (retain, nonatomic) IBOutlet UILabel *callStatus;
- (IBAction) onButtonClick: (id)sender;
- (IBAction) onButtonNumpadClick: (id)sender;
- (IBAction)touchedDown:(id)sender;
-(void) updateViewAndState;
@end
