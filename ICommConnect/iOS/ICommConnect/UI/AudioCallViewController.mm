//
//  AudioCallViewController.m
//  ICommConnect
//
//  Created by Ravikanth on 9/3/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//
#import "AudioCallViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ICommConnectAppDelegate.h"
#import "MediaContent.h"
#import "MediaSessionMgr.h"
#import "Constants.h"
#import "RestApi+SIPSettings.h"
#import "RestApi+GetActiveCallsCount.h"
#import "CurrentLocation.h"
#import "RestApi+GetMyIP.h"
#import "RestApi+CanAllowCall.h"
#import "RestApi+VerifyPlanMinutes.h"
#define kNetworkAlertMsgThreedGNotEnabled			@"Only 3G network is available. Please enable 3G and try again."
#define kNetworkAlertMsgNotReachable				@"No network connection"
#define kAlertMsgButtonOkText						@"OK"
#define kAlertMsgButtonCancelText					@"Cancel"
static BOOL kEnableEarlyIMS = TRUE;
/*=== AudioCallViewController (Private) ===*/
@interface AudioCallViewController(Private)
+(void) applyGradienWithColors: (NSArray*)colors forView: (UIView*)view_ withBorder:(BOOL)border;
-(void) closeView;
-(void) animateViewCenter;
-(void) networkAlert:(NSString*)message;
@end
/*=== AudioCallViewController (Timers) ===*/
@interface AudioCallViewController (Timers)
-(void)timerInCallTick:(NSTimer*)timer;
-(void)timerSuicideTick:(NSTimer*)timer;

@end
/*=== AudioCallViewController (SipCallbackEvents) ===*/
@interface AudioCallViewController(SipCallbackEvents)
-(void) onInviteEvent:(NSNotification*)notification;
-(void) onNetworkEvent:(NSNotification*)notification;
@end

// private properties
@interface AudioCallViewController()
@property(nonatomic) BOOL numpadIsVisible;
@end
//
//	AudioCallViewController(Private)
//
@implementation AudioCallViewController(Private)
-(void) networkAlert:(NSString*)message{
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iDoubs"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:kAlertMsgButtonOkText
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}
//applying background color on selection of a prticular button
+(void) applyGradienWithColors: (NSArray*)colors forView: (UIView*)view_ withBorder:(BOOL)border{
    for(CALayer *ly in view_.layer.sublayers){
        if([ly isKindOfClass: [CAGradientLayer class]]){
            [ly removeFromSuperlayer];
            break;
        }
    }
    if(colors){
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.colors = colors;
        gradient.frame = CGRectMake(0.f, 0.f, view_.frame.size.width, view_.frame.size.height);
        if(border){
            gradient.cornerRadius = 8.f;
            gradient.borderWidth = 2.f;
        }else{
        gradient.cornerRadius = 25.f;
        }
        view_.backgroundColor = [UIColor clearColor];
        [view_.layer insertSublayer:gradient atIndex:0];
    }
}
//closing the view
-(void) closeView{
    [self dismissViewControllerAnimated:YES completion:nil];
}
//animating number pad
-(void) animateViewCenter{
    self.viewNumpad.hidden=NO;
    [UIView beginAnimations:@"animateViewCenter" context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:self.viewCenter
                             cache:YES];
//    for(UIView *view in self.viewCenter.subviews){
//        [view removeFromSuperview];
//    }
    [self.viewCenter addSubview:numpadIsVisible ? self.viewNumpad : self.profileImageView];
    [UIView commitAnimations];
}

@end
@implementation AudioCallViewController(SipCallbackEvents)
//== Network events == //
-(void) onNetworkEvent:(NSNotification*)notification {
    NgnNetworkEventArgs *eargs = [notification object];
    
    switch (eargs.eventType) {
        case NETWORK_EVENT_STATE_CHANGED:
        default:
        {
            NSLog(@"NetworkEvent reachable=%@ networkType=%i",
                     [NgnEngine sharedInstance].networkService.reachable ? @"YES" : @"NO", [NgnEngine sharedInstance].networkService.networkType);
            
            if([NgnEngine sharedInstance].networkService.reachable){
                BOOL onMobileNework = ([NgnEngine sharedInstance].networkService.networkType & NetworkType_WWAN);
                
                if(onMobileNework){ // 3G, 4G, EDGE ...
                    MediaSessionMgr::defaultsSetBandwidthLevel(tmedia_bl_medium); // QCIF, SQCIF
                }
                else {// WiFi
                    MediaSessionMgr::defaultsSetBandwidthLevel(tmedia_bl_unrestricted);// SQCIF, QCIF, CIF ...
                }
                
                // unregister the application and schedule another registration
                BOOL on3G = onMobileNework; // Downgraded to 3G even if it could be 4G or EDGE
                BOOL use3G = [[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_3G];
                if(on3G && !use3G){
                    [self networkAlert:kNetworkAlertMsgThreedGNotEnabled];
                    [[NgnEngine sharedInstance].sipService stopStackSynchronously];
                }
                else { // "on3G and use3G" or on WiFi
                    // stop stack => clean up all dialogs
                    [[NgnEngine sharedInstance].sipService stopStackSynchronously];
                    [[NgnEngine sharedInstance].sipService registerIdentity];
                }
            }
            else{
                if([NgnEngine sharedInstance].sipService.registered){
                    [[NgnEngine sharedInstance].sipService stopStackSynchronously];
                }
            }
            
            break;
        }
    }
}

//Invite event will fire call events
-(void) onInviteEvent:(NSNotification*)notification {
    NgnInviteEventArgs* eargs = [notification object];
    if(!audioSession || audioSession.id != eargs.sessionId){
        return;
    }
    
    NSLog(@"case:%u",eargs.eventType);
    switch (eargs.eventType) {
        case INVITE_EVENT_INPROGRESS:
        case INVITE_EVENT_INCOMING:
        case INVITE_EVENT_RINGING:
        case INVITE_EVENT_LOCAL_HOLD_OK:
        case INVITE_EVENT_REMOTE_HOLD:
        default:
        {
            // updates view and state
            [self updateViewAndState];
            break;
        }
          case INVITE_EVENT_REMOTE_TRANSFER_REQUESTED:
        {
            
            NSString *remoteUri=[[[[eargs.remoteUri componentsSeparatedByString:@"@"] objectAtIndex:0] componentsSeparatedByString:@":"] objectAtIndex:1];
            NSLog(@"parsed remoteUri:%@",remoteUri);
            [audioSession hangUpCall];
            [self invokeNewCallToTheNumber:remoteUri];
            break;
        }
            // transilient events
        case INVITE_EVENT_MEDIA_UPDATING:
        case INVITE_EVENT_MEDIA_UPDATED:
        case INVITE_EVENT_TERMINATED:
        case INVITE_EVENT_TERMWAIT:
        {
            // updates view and state
            [self updateViewAndState];
            // releases session
            [NgnAVSession releaseSession: &audioSession];
            // starts timer suicide
            [NSTimer scheduledTimerWithTimeInterval: kCallTimerSuicide
                                             target: self
                                           selector: @selector(timerSuicideTick:)
                                           userInfo: nil
                                            repeats: NO];
            [self closeView];
            break;
        }
    }
}
-(void) onRegistrationEvent:(NSNotification*)notification {
    NgnRegistrationEventArgs* eargs = [notification object];
    
    // Current event triggered the callback
    // to get the current registration state you should use "mSipService::getRegistrationState"
    switch (eargs.eventType) {
            // provisional responses
        case REGISTRATION_INPROGRESS:
        case UNREGISTRATION_INPROGRESS:
            
            break;
            // final responses
        case REGISTRATION_OK:
            [self invokeCall];
            break;
        case REGISTRATION_NOK:
            self.callStatus.text = @"Registration Failed...!";
            [self closeView];
            break;
        case UNREGISTRATION_OK:
        case UNREGISTRATION_NOK:
        default:
            
            break;
    }
}
//clearing the earlier audio session and creating a new session before making a call and then will call to the number.
-(void)invokeCall{
    
    NSString *callerNumber;
    if([self.contactDictionary objectForKey:@"ContactNumbers"]){
        callerNumber=[[[self.contactDictionary objectForKey:@"ContactNumbers"] objectAtIndex:self.contactIndex] objectForKey:@"ContactNo"];
    }else{
        callerNumber=[self.contactDictionary objectForKey:@"DefaultContact"];
    }
    self.callStatus.text = @"Registered...!";
    [audioSession release];
    [self invokeNewCallToTheNumber:callerNumber];
}
-(void)invokeNewCallToTheNumber:(NSString*)number{
    audioSession=[[NgnAVSession makeAudioCallWithRemoteParty:number andSipStack: [[NgnEngine sharedInstance].sipService getSipStack]] retain];
    if(audioSession){
        [[NgnEngine sharedInstance].soundService setSpeakerEnabled:[audioSession isSpeakerEnabled]];
        [self updateViewAndState];
    }

}
@end
//
// AudioCallViewController (Timers)
//
@implementation AudioCallViewController (Timers)

-(void)timerInCallTick:(NSTimer*)timer{
    // to be implemented for the call time display
}

-(void)timerSuicideTick:(NSTimer*)timer{
    [self performSelectorOnMainThread:@selector(closeView) withObject:nil waitUntilDone:NO];
}

@end
@implementation AudioCallViewController
@synthesize buttonHangup;
@synthesize buttonMute;
@synthesize buttonNumpad;
@synthesize contactImageView;
@synthesize profileImageView;
@synthesize viewNumpad;
@synthesize viewCenter;
@synthesize viewBottom;
@synthesize callStatus;
@synthesize buttonSpeaker;
@synthesize numpadIsVisible;
@synthesize sessionId;
@synthesize contactDictionary;
@synthesize CallToTheNumber;
@synthesize imageData;
@synthesize contactIndex;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        numpadIsVisible = NO;
    }
    return self;
}
//Updating the Status of the call
-(void) updateViewAndState{
    if(audioSession){
        switch (audioSession.state) {
            case INVITE_STATE_INPROGRESS:
            {
                self.callStatus.text = @"Calling...";
                self.buttonHangup.hidden = NO;
                buttonMute.enabled= YES;
                buttonSpeaker.enabled=YES;
                break;
            }
            case INVITE_STATE_INCOMING:
                 break;
            case INVITE_STATE_REMOTE_RINGING:
            {
                self.callStatus.text = @"Remote is ringing";
                self.buttonHangup.hidden = NO;
                [[NgnEngine sharedInstance].soundService playRingBackTone];
                break;
            }
            case INVITE_STATE_INCALL:
            {
                self.callStatus.text = @"In Call";
                self.buttonNumpad.enabled=YES;
                self.buttonNumpad.alpha=1.0;
                self.buttonHangup.hidden = NO;
                [[NgnEngine sharedInstance].soundService stopRingBackTone];
                [[NgnEngine sharedInstance].soundService stopRingTone];
                [self autoDialExtension];
                [self verifyPlanMinutes];//this api called to invoke verifyPlanMinutes Job
                break;
            }
            case INVITE_STATE_TERMINATED:
            case INVITE_STATE_TERMINATING:
            {
                self.callStatus.text = @"Terminating...";
                self.buttonHangup.hidden = YES;
                [[NgnEngine sharedInstance].soundService stopRingBackTone];
                [[NgnEngine sharedInstance].soundService stopRingTone];
                break;
            }
            default:
                break;
        }
        [AudioCallViewController applyGradienWithColors: [audioSession isSpeakerEnabled] ? kColorsBlue : nil
                                                forView:self.buttonSpeaker withBorder:NO];
        
        [AudioCallViewController applyGradienWithColors: [audioSession isMuted] ? kColorsBlue : nil forView:self.buttonMute withBorder:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //Registering the events
     [self loadViewbackgroundColor];
     [self setNavigationBarSettings];
     [self getCallLogs];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onNetworkEvent:) name:kNgnNetworkEventArgs_Name object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRegistrationEvent:) name:kNgnRegistrationEventArgs_Name object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onInviteEvent:) name:kNgnInviteEventArgs_Name object:nil];
    [self.profileImageView setBackgroundColor:[UIColor colorFromHexString:[[[NSUserDefaults standardUserDefaults ] objectForKey:@"DirectoryBackgroundColor"] stringByReplacingOccurrencesOfString:@"#" withString:@""]]];
}
- (void)viewWillAppear:(BOOL)animated{
    self.viewNumpad.hidden=YES;
    self->numpadIsVisible = NO;
     [AudioCallViewController applyGradienWithColors: numpadIsVisible ? kColorsBlue : nil forView:self.buttonNumpad withBorder:NO];
    [self.viewCenter addSubview:profileImageView];
    self.buttonNumpad.enabled=NO;
    
    @try {
        if (imageData !=(NSData *)nil) {
            contactImageView.image=[UIImage imageWithData:imageData];
        }else{
            contactImageView.image=[UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"logoImageData"]];
        }
    }
    @catch (NSException *exception) {
         contactImageView.image=[UIImage imageNamed:@"NoImage.png"];
    }

    if([contactDictionary objectForKey:@"ContactName"]){
        self.contactName.text=[contactDictionary objectForKey:@"ContactName"];
    }else{
        self.contactName.text=[contactDictionary objectForKey:@"DefaultContactLabel"];
    }
    [self getMyDeviceNetworkPublicIP];
}
//this service called to get public router IP of device network.
-(void)getMyDeviceNetworkPublicIP{
     [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    [[RestApi alloc] getDevicePublicIP:self];
}
//in that we will get IP address of device network with that we will know whether that IP is blocked or not, If it is blocked we won't allow call.
-(void)handleGetMyIPResult:(NSDictionary *)responseDictionary{
    [SVProgressHUD dismiss];
    if(![responseDictionary isKindOfClass:[NSNull class]]){
        [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
        [[RestApi alloc] getIPStatusToAllowCall:self forSubscriptionGuId:[self getSubscriptionGUID] withIPAddres:[responseDictionary objectForKey:@"IP"]];
    }
}
//handling Can call result, based on this will allow to make call
-(void)handleCanAllowCallResult:(NSDictionary *)responseDictionary{
    [SVProgressHUD dismiss];
    if(![responseDictionary isKindOfClass:[NSNull class]]){
        //checking whether call can be allowed for subscription id
        if([[responseDictionary objectForKey:@"CanAllow"] boolValue]){
            [self getActiveCallsCount];
        }else{
            [self createAlertWith:IPBlocked_Message tag:2];
        }
    }
}
-(void)createAlertWith:(NSString*)message tag:(int)alertTag{
    UIAlertView *alert = [[[UIAlertView alloc] init] autorelease];
    alert.delegate=self;
    [alert setTitle:@"Alert"];
    [alert setMessage:message];
    [alert addButtonWithTitle:@"OK"];
    alert.tag=alertTag;
    [alert show];
}
//handling alert button actions
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==2){
        [self closeView];
    }
}
//clearing logs
-(void)clearLogs{
    NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
    [removeUD removeObjectForKey:@"locationValues"];
    [removeUD removeObjectForKey:@"IPAddress"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
}
//getting call logs
-(void)getCallLogs{
    [self clearLogs];
    NSString *str=[[CurrentLocation sharedInstance] findCurrentLocation];
     NSLog(@"loc value:%@",str);
    [self setLocalObject:str forKey:@"locationValues"];
   // [self setLocalObject:[self getIPAddress] forKey:@"IPAddress"];

}
//Calling a web api to get active calls count
-(void)getActiveCallsCount{
  
    NSLog(@"retain count:%lu",(unsigned long)self.retainCount);
    if (self.retainCount<=1) {
        [[RestApi alloc] getActiveCallsCount:[self retain]forSubscriptionGuId:[self getSubscriptionGUID]];
    }else{
    [[RestApi alloc] getActiveCallsCount:self forSubscriptionGuId:[self getSubscriptionGUID]];
    }    // [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];

    
}
//this service will be called whnever call is i INCALL, to verify plan remainng minutes to email the status to client.
-(void)verifyPlanMinutes{
[[RestApi alloc] callJobVerifyPlanMinutes:self forSubscriberId:[self getSubscriptionGUID]];
}
//Calling a web api to get subscriber sip details
-(void)getSIPSettingsDetails{
     [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    if (self.retainCount<=1) {
    
         [[RestApi alloc] getSIPSettings:[self retain] withSubscriptionGuId:[self getSubscriptionGUID]];
    }else{
      [[RestApi alloc] getSIPSettings:self withSubscriptionGuId:[self getSubscriptionGUID]];
    }
   
   
    
}
//handling the  sip details api results
-(void)handleSIPResult:(NSMutableDictionary *)responseDictionary{
    [SVProgressHUD dismiss];
    if([responseDictionary count]>0){
        [self registerSubscriberwithSIPDetails:responseDictionary];
    }else{
        [self showAlertViewWithTitle:@"Message" withMessage:@"Sorry, subscriber details are unavailable."];
        [self closeView];
    }
}

//handling the  get active calls count result,in that if all lines are busy will connect user by checking for every 10 sec.
-(void)handleActiveCallsResult:(NSDictionary *)responseDictionary{
    [SVProgressHUD dismiss];

    if(audioSession){
        audioSession=nil;
    }
    
    if([responseDictionary count]>0){
        if ([[responseDictionary objectForKey:@"AllowedConcurrentCalls"] isKindOfClass:[NSNull class]]) {
             self.callStatus.text =@"Connecting ..!";
            [self getSIPSettingsDetails];//once lines are ready to connectwill call the api to get sip settings
        }else{
            int activeCalls=[[responseDictionary objectForKey:@"ActiveCalls"] intValue];
            int allowedconcurrentcalls=[[responseDictionary objectForKey:@"AllowedConcurrentCalls"] intValue];
            NSLog(@"calls:%d,%d",activeCalls,allowedconcurrentcalls);
            if (activeCalls<allowedconcurrentcalls) {
                self.callStatus.text =@"Connecting ..!";
                [self getSIPSettingsDetails];//once lines are ready to connectwill call the api to get sip settings

            }else{
                self.callStatus.text =LinesBusy_Message;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, apiCallIntervel* NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self getActiveCallsCount];//verifying active lines status

                });
            }
        }

    }

}
//registering the sip details of subscriber
-(void)registerSubscriberwithSIPDetails:(NSMutableDictionary*)sipDictionary{
    self.callStatus.text = @"Registering...!";
    // add observers
    // take an instance of the engine
    [NgnEngine initialize];
    mEngine = [[NgnEngine sharedInstance] retain];
    
    // take needed services from the engine
    mSipService = [mEngine.sipService retain];
    mConfigurationService = [mEngine.configurationService retain];
    // start the engine
    [mEngine start];
    NSArray *proxyArray=[self splitTheString:[sipDictionary objectForKey:@"SIPUDP"]];
    // set credentials
    NSString *publicIdentity=[sipDictionary objectForKey:@"PublicIdentity"];
    [mConfigurationService setStringWithKey: IDENTITY_IMPI andValue: [sipDictionary objectForKey:@"PrivateIdentity"]];
    [mConfigurationService setStringWithKey: IDENTITY_IMPU andValue: publicIdentity];
    [mConfigurationService setStringWithKey: IDENTITY_PASSWORD andValue: [sipDictionary objectForKey:@"Password"]];
    
    [mConfigurationService setStringWithKey: NETWORK_REALM andValue: [[publicIdentity componentsSeparatedByString:@"@"] objectAtIndex:1]];
    [mConfigurationService setStringWithKey: NETWORK_PCSCF_HOST andValue:proxyArray[0]];
     [mConfigurationService setIntWithKey: NETWORK_PCSCF_PORT andValue: [proxyArray[1] intValue]];
//    [mConfigurationService setStringWithKey: NETWORK_PCSCF_HOST andValue:@"192.168.1.9"];
//    [mConfigurationService setIntWithKey: NETWORK_PCSCF_PORT andValue:5070];
    [mConfigurationService setBoolWithKey: NETWORK_USE_EARLY_IMS andValue: kEnableEarlyIMS];
    [mConfigurationService setStringWithKey: IDENTITY_DISPLAY_NAME andValue: [sipDictionary objectForKey:@"DisplayName"]];
    // Override point for customization after application launch
    
    // Try to register the default identity
    [mSipService registerIdentity];
}
//Unregistering the sip details, this will be invoked once the call is ended or terminated.
-(void)unregisterSubscriber{
    mSipService=nil;
    [mSipService unRegisterIdentity];
    // mSipService=nil;
    
    [mEngine stop];
//    [mEngine release];
//    [mSipService release];
//    [mConfigurationService release];
}
//auto dialing extensions with delay.Here Each digit will be send to DTMF with delay of 1 sec and 2.5 sec delay for each comma.
-(void)autoDialExtension{
    if([self.contactDictionary objectForKey:@"ContactNumbers"] && ![[[[self.contactDictionary objectForKey:@"ContactNumbers"] objectAtIndex:self.contactIndex] objectForKey:@"Extensions"] isKindOfClass:[NSNull class]]){
        NSString *extensions= [[[self.contactDictionary objectForKey:@"ContactNumbers"] objectAtIndex:self.contactIndex] objectForKey:@"Extensions"];
        float myDelay = 2.5;
        BOOL NoComma=TRUE;
        for( int indexValue=0; indexValue<extensions.length; indexValue++){
            NSString *currentStr=[NSString stringWithFormat:@"%c",[extensions characterAtIndex:indexValue]];
            if([currentStr isEqualToString:@","]){
                
                myDelay += 2.5;//adding 2.5 sec delay for a comma
                if (NoComma) {
                    myDelay -=1.0;
                }
                NoComma=FALSE;
            }else{
                if([currentStr isEqualToString:@"#"]){
                    currentStr=@"11";//sending 11 as the value of # symbol
                }
                if([currentStr isEqualToString:@"*"]){
                    currentStr=@"10";//sending 10 as the value of * symbol
                }
                NoComma=TRUE;
                [self performSelector:@selector(sendDigitToDtmf:) withObject:currentStr afterDelay:myDelay];
                myDelay = myDelay  + 1.0;//adding 1.0 sec delay in between digits
            }
        }
    }
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIDevice currentDevice].proximityMonitoringEnabled = YES;
}
//releasing the session and unregistering while dispperaing from screen
- (void)viewWillDisappear:(BOOL)animated{
    [NgnAVSession releaseSession: &audioSession];
    [self unregisterSubscriber];
    [UIDevice currentDevice].proximityMonitoringEnabled = NO;
}
-(void)viewDidDisappear:(BOOL)animated{
    [self clearLogs];
    //[[ICommConnectAppDelegate sharedInstance] unregisterSubscriber];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
//showing and hiding number pad with animation
-(void) setNumpadIsVisible:(BOOL)visible{
    self->numpadIsVisible = visible;
    [self animateViewCenter];
}
//handling events of Call buttons
- (IBAction) onButtonClick: (id)sender{
    
    if(audioSession){
        if(sender == buttonHangup){
            [audioSession hangUpCall];
        }
        else if(sender == buttonMute){
            if([audioSession setMute:![audioSession isMuted]]){
                self.buttonMute.selected = [audioSession isMuted];
                [AudioCallViewController applyGradienWithColors: [audioSession isMuted] ? kColorsBlue : nil forView:self.buttonMute withBorder:NO];
            }
        }
        else if(sender == buttonSpeaker){
            [audioSession setSpeakerEnabled:![audioSession isSpeakerEnabled]];
            if([[NgnEngine sharedInstance].soundService setSpeakerEnabled:[audioSession isSpeakerEnabled]]){
                self.buttonSpeaker.selected = [audioSession isSpeakerEnabled];
                [AudioCallViewController applyGradienWithColors: [audioSession isSpeakerEnabled] ? kColorsBlue : nil forView:self.buttonSpeaker withBorder:NO];
            }
        }
        else if(sender == buttonNumpad){
            numpadIsVisible?self.numpadIsVisible=NO: self.numpadIsVisible = YES;
            [AudioCallViewController applyGradienWithColors: numpadIsVisible ? kColorsBlue : nil forView:self.buttonNumpad withBorder:NO];
        }
        
    }else{
        if(sender == buttonHangup){
            [self closeView];
        }

    }
}
//handling events of number pad button clicks
- (IBAction) onButtonNumpadClick: (id)sender{
    if(audioSession){
        UIButton *NumberButton=(UIButton*)sender;
        NumberButton.backgroundColor=[UIColor clearColor];
        int tag = (int)((UIButton*)sender).tag;
        [self sendDtmfValues:tag];
    }
}
//highlighting button on toch
- (IBAction)touchedDown:(id)sender{
    UIButton *NumberButton=(UIButton*)sender;
    NumberButton.layer.cornerRadius=10;
    NumberButton.backgroundColor=[UIColor lightGrayColor];
}
//preparing digits to send DTMF
-(void)sendDigitToDtmf:(NSString*)digit{
    [self sendDtmfValues:[digit intValue]];
}
//sending digits to DTMF and playing sounds
-(void)sendDtmfValues:(int)dtmfValue{
    [audioSession sendDTMF:dtmfValue];
    [[NgnEngine sharedInstance].soundService playDtmf:dtmfValue];
}
- (void)dealloc {
    [audioSession release];
    [buttonHangup release];
    [buttonMute release];
    [buttonNumpad release];
    [viewCenter release];
    [viewBottom release];
    [viewNumpad release];
    [contactImageView release];
    [callStatus release];
    [buttonSpeaker release];
    [profileImageView release];
    [super dealloc];
}
@end
