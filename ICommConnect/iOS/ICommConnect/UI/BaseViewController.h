//
//  BaseViewController.h
//  subscription
//
//  Created by Ravikanth on 8/20/15.
//  Copyright (c) 2015 PRIME. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iOSNgnStack.h"
#import "UIColor+StringtoColor.h"
#import "RestDelegate.h"
#import "SVProgressHUD.h"
//#import "GlobalData.h"
#import "Constants.h"
@interface BaseViewController : UIViewController<RestDelegate>
@property(nonatomic,retain)NSMutableDictionary *imageDictionary;
-(float)designCustomView:(UIView*)receivedView withDepartment:(NSArray*)departmentGroup title:(NSString*)departmentTitle;
-(id)groupTheDepartmentsByGroupName:(NSMutableDictionary*)receivedDictionary;
-(id)sortTheArray:(NSMutableArray*)receivedArray;
-(void)loadViewbackgroundColor;
-(void)setNavigationBarSettings;
-(void)setLocalObject:(id)object forKey:(NSString*)Key;
-(void)showAlertViewWithTitle:(NSString*)title withMessage:(NSString*)message;
-(id)splitTheString:(NSString*)receivedString;
- (NSString *)getIPAddress;
-(NSString*)getSubscriptionGUID;
-(id)getObjectForKey:(NSString*)key;
@property(nonatomic,retain)NSUserDefaults *standardUserDefaults;
@property(nonatomic,retain)NSString *subscription_ID;
@end

