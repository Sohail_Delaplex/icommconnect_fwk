//
//  BaseViewController.m
//  subscription
//
//  Created by Ravikanth on 8/20/15.
//  Copyright (c) 2015 PRIME. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "BaseViewController.h"
#import "ICommConnectAppDelegate.h"
#import "Constants.h"
@interface BaseViewController (){
    float  buttonWidth;
    float  buttonHeight;
}
@end
@implementation BaseViewController
@synthesize standardUserDefaults;
@synthesize subscription_ID;
@synthesize imageDictionary;
- (void)viewDidLoad {
    [super viewDidLoad];
    imageDictionary=[[NSMutableDictionary alloc] init];
    standardUserDefaults=[NSUserDefaults standardUserDefaults];
    subscription_ID=[standardUserDefaults objectForKey:@"SubscriptionGUId"];
   // [self setViewbackgroundColor:[standardUserDefaults objectForKey:@"DirectoryBackgroundColor"]];
   // [self setNavigationBarSettings];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewDidUnload {
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
- (void)dealloc {
    [super dealloc];
}
//setting view background color
-(void)loadViewbackgroundColor{
    NSLog(@"cc:%@",[standardUserDefaults objectForKey:@"DirectoryBackgroundColor"]);
    UIColor *backgroundColor=[UIColor colorFromHexString:[[standardUserDefaults objectForKey:@"DirectoryBackgroundColor"] stringByReplacingOccurrencesOfString:@"#" withString:@""]];
    [self.view setBackgroundColor:backgroundColor];
    [[UINavigationBar appearance] setBarTintColor:backgroundColor];
}
//setting view background color
-(void)setNavigationBarSettings{
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleBordered target:self action:@selector(bacButtonClicked:)];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [[UINavigationBar appearance] setBarTintColor:[UIColor colorFromHexString:[[standardUserDefaults objectForKey:@"DirectoryBackgroundColor"] stringByReplacingOccurrencesOfString:@"#" withString:@""]]];
    [barButtonItem release];
}
//this metjod will fire when back button tapped on navigation bar.
-(void)bacButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
/*this method will design the view with number of tiles based on their group name, Tiles will be attached to view dynamically based on their width and height.
*/
-(float)designCustomView:(UIView*)receivedView withDepartment:(NSArray*)departmentGroup title:(NSString*)departmentTitle{
    int screenWidth=self.view.frame.size.width;
    int spaceBetweenButtons=10;//Space between two tiles
    int titleLabelHeight=30;//Tile Header
    int paddingToBorder=20;//padding to border
    int paddingToTop=5;//padding to top tile
    int paddingToBottom=5;//padding to bottom tile   ##@
    int numberOfDepartments=(int)[departmentGroup count];// Number of groups
    int xIncrement=0;
    int yIncrement=0;
    float contentheight;
    buttonWidth=boxWidth;
    buttonHeight=boxHeight;
    
    UILabel *headingLabel=[[UILabel alloc] init];//Group heading title
    if (departmentTitle.length<1 ||[departmentTitle isEqualToString:@"##@"]) {
        titleLabelHeight=0;
    }else{
        headingLabel.text=departmentTitle;
    }
    headingLabel.frame=CGRectMake(paddingToBorder, paddingToTop,screenWidth-paddingToBorder, titleLabelHeight);
    headingLabel.textColor=[UIColor whiteColor];
    headingLabel.font=headingFont;
    paddingToTop+=titleLabelHeight+spaceBetweenButtons;//this is Y position of next row
    
    int buttonsPerRow=(screenWidth-2*paddingToBorder)/(buttonWidth+spaceBetweenButtons);//number of buttons per a row
    int remainingSpace=screenWidth-2*paddingToBorder-(buttonWidth*buttonsPerRow)-(spaceBetweenButtons*(buttonsPerRow-1));//calculating the white space
    buttonWidth=buttonWidth+(remainingSpace/buttonsPerRow);//filling the white space by increasing button width, adding that white soace equally among the buttons.
    [receivedView addSubview:headingLabel];
    for (int i = 0; i<numberOfDepartments; i++){
        if (screenWidth-2*paddingToBorder-buttonWidth*xIncrement- xIncrement*spaceBetweenButtons<buttonWidth) {
            yIncrement++;
            xIncrement=0;
        }
        UIButton *tile=[[[UIButton alloc] initWithFrame:CGRectMake(paddingToBorder +buttonWidth*xIncrement +xIncrement*spaceBetweenButtons,buttonHeight*yIncrement+paddingToTop+ yIncrement*spaceBetweenButtons, buttonWidth,buttonHeight)] autorelease];//declaration of a Tile
        xIncrement++;
        tile.layer.cornerRadius = 10;
        tile.clipsToBounds = YES;
        if ([departmentTitle isEqualToString:@"##@"]) {
            tile.backgroundColor=[UIColor lightGrayColor];
            tile.tag=i;
            [tile setTitle:[[departmentGroup objectAtIndex:i] objectForKey:@"Domain"] forState:UIControlStateNormal];
            @try {
                NSString *imageUrl=[[departmentGroup objectAtIndex:i] objectForKey:@"Logo"];
                NSMutableDictionary *dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"imageChache"];
                if (![dict isKindOfClass:[NSNull null]] && [dict objectForKey:[NSString stringWithFormat:@"%d",i]]) {
                    [tile setBackgroundImage:[UIImage imageWithData:[dict objectForKey:[NSString stringWithFormat:@"%d",i]]] forState:UIControlStateNormal];
                    [tile setTitle:@"" forState:UIControlStateNormal];
                }else{
                    if (![imageUrl isKindOfClass:[NSNull class]]) {
                        dispatch_queue_t DownloadQueue = dispatch_queue_create("Download Pic", NULL);
                        dispatch_async(DownloadQueue, ^{
                            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[departmentGroup objectAtIndex:i] objectForKey:@"Logo"]]];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [imageDictionary setValue:imageData forKey:[NSString stringWithFormat:@"%d",i]];
                                 if (![imageData isKindOfClass:[NSNull class]]) {
                                [tile setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
                                 }
                                [tile setTitle:@"" forState:UIControlStateNormal];
                                [[NSUserDefaults standardUserDefaults] setObject:imageDictionary forKey:@"imageChache"];
                            });
                        });
                    }
                }
            }
            @catch (NSException *exception) {
                [tile setTitle:[[departmentGroup objectAtIndex:i] objectForKey:@"Domain"] forState:UIControlStateNormal];
            }
            [SVProgressHUD dismiss];
            
        }else{
            //setting directory or subdirectories background image
            NSString *backgroundImageUrl=[[departmentGroup objectAtIndex:i] objectForKey:@"DepartmentLogo"];
            if (![backgroundImageUrl isKindOfClass:[NSNull class]]) {
               [self setImageWithUrl:backgroundImageUrl forTile:tile];
            }
            //setting directory background color
            tile.backgroundColor=[UIColor colorFromHexString:[[standardUserDefaults objectForKey:@"DirectoryiconColor"] stringByReplacingOccurrencesOfString:@"#" withString:@""]];
            NSString *shortName=[[departmentGroup objectAtIndex:i] objectForKey:@"ShortName"];
            if(![shortName isKindOfClass:[NSNull class]]){
            [tile setTitle:shortName forState:UIControlStateNormal];
            }
          //  NSLog(@"iiid:%ld",[[[departmentGroup objectAtIndex:i] objectForKey:@"ObjectID"] integerValue]);
            [tile setTag:[[[departmentGroup objectAtIndex:i] objectForKey:@"ObjectID"] integerValue]];
        }
        
        tile.imageView.contentMode=UIViewContentModeScaleAspectFit;
        tile.titleLabel.font=titleFont;
        tile.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        tile.titleLabel.numberOfLines = 3;
        tile.titleLabel.textAlignment=NSTextAlignmentCenter;
        [tile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
        [tile addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [receivedView addSubview:tile];//adding tile to the view
    }
    
    contentheight=paddingToTop+paddingToBottom+(yIncrement+1)*buttonHeight+yIncrement*spaceBetweenButtons;//overall Content height
    receivedView.frame=CGRectMake(0, 0, screenWidth, contentheight);
    return contentheight;
}
-(void)setImageWithUrl:(NSString*)url forTile:(UIButton*)tile{
    dispatch_queue_t DownloadQueue = dispatch_queue_create("Download Pic", NULL);
    dispatch_async(DownloadQueue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![imageData isKindOfClass:[NSNull class]]) {
                [tile setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
            }
            
        });
    });
}
-(void)btnAction:(id)sender{
}
/* This method will segregate the objects and grouped the similar objects with their GroupName */
-(id)groupTheDepartmentsByGroupName:(NSMutableDictionary*)receivedDictionary{
    NSMutableDictionary *sortDictionary = [[NSMutableDictionary dictionary] retain];
    NSMutableArray *nullObjectsArray=[[[NSMutableArray alloc]init]autorelease];
    for (id obj in receivedDictionary) {
        id keyValue = [obj valueForKey:@"GroupName"];
        NSMutableArray *sortArray = sortDictionary[keyValue];
        if (! sortArray) {
            sortArray = [NSMutableArray array];
            if([keyValue isKindOfClass:[NSNull class]]){
                [nullObjectsArray addObject:obj];
            }else{
                sortDictionary[keyValue] = sortArray;
            }
        }
        [sortArray addObject:obj];
    }
    if([nullObjectsArray count]>0){
        [sortDictionary setObject:nullObjectsArray forKey:@""];
    }
    
    return sortDictionary;
}
/* This method will sort the objects of an array*/
-(id)sortTheArray:(NSMutableArray*)receivedArray{
    for(int i = 0; i < receivedArray.count; i++){
        if([receivedArray[i] isKindOfClass:[NSNull class]]){
            receivedArray[i] = @"";
        }
    }
    [receivedArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return receivedArray;
}
/* This method will split the given url string to get proxy host and port number*/
-(id)splitTheString:(NSString*)receivedString{
    receivedString=[receivedString stringByReplacingOccurrencesOfString:@"udp://" withString:@""];
    return [receivedString componentsSeparatedByString:@":"];
}
/*setting a local object using defaults */
-(void)setLocalObject:(id)object forKey:(NSString*)Key{
    [standardUserDefaults setObject:object forKey:Key];
}
/*getting the local object using defaults */
-(id)getObjectForKey:(NSString*)key{
    return [standardUserDefaults objectForKey:key];
}
/*Common method to show an aletr */
-(void)showAlertViewWithTitle:(NSString*)title withMessage:(NSString*)message{
    UIAlertView *alert = [[[UIAlertView alloc] init] autorelease];
    [alert setTitle:title];
    [alert setMessage:message];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
   	[textField resignFirstResponder];
    return YES;
}
- (void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *)event {
    [self.view endEditing:YES];
}
//Error Handling
- (void) handleError:(NSError *) error withStatus:(int) statusCode{
    [SVProgressHUD dismiss];
    NSString *errorMessage=[error localizedDescription];
    if([errorMessage isEqualToString:@"Could not connect to the server."]){
        [self setLocalObject:@"Failed" forKey:@"Connection"];
        errorMessage=[NSString stringWithFormat:@"Could not connect to the server, please check your internet connection."];
    }
    NSString *title=@"Network Error";
    if (statusCode == 500) {
        errorMessage=@"Error";
    } else if (statusCode == 404) {
        errorMessage=@"Service Unavailable.";
    }else if(statusCode == 400){
    errorMessage=@"Sorry for the inconvenience, an error occurred while processing your request.";
    }
    [self showAlertViewWithTitle:title withMessage:errorMessage];
}
-(NSString*)getSubscriptionGUID{
    return  [self.standardUserDefaults objectForKey:@"SubscriptionGUId"];
}

//to get IP address of Phone
/*
- (NSString *)getIPAddress {
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    // retrieve the current interfaces - returns 0 on success
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]; // pdp_ip0
                NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
                
                if([name isEqualToString:@"en0"]) {
                    // Interface is the wifi connection on the iPhone
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        // Interface is the cell connection on the iPhone
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    NSString *addr = wifiAddress ? wifiAddress : cellAddress;
    return addr ? addr : @"0.0.0.0";
    
}
*/
@end
