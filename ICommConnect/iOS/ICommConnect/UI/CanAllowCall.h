//
//  CanAllowCall.h
//  ICommConnect
//
//  Created by Ravikanth on 2/19/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CanAllowCall <NSObject>
- (void) handleCanAllowCallResult:(NSDictionary*)responseDictionary;
@end
