//
//  ContactDetailsDelegate.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ContactDetailsDelegate <NSObject>
- (void) handleContactDetailsResult:(NSMutableDictionary*)responseDictionary;
@end
