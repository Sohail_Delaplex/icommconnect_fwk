//
//  ContactViewController.h
//  ICommConnect
//
//  Created by Ravikanth on 9/3/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "BaseViewController.h"
#import "ContactDetailsDelegate.h"
#import "SubscriberInfoDelegate.h"
@interface ContactViewController : BaseViewController<SubscriberInfoDelegate,ContactDetailsDelegate>
@property (retain, nonatomic) IBOutlet UIView *dataView;
@property (retain, nonatomic) IBOutlet UIImageView *contactImageView;
@property (retain, nonatomic) IBOutlet UILabel *contactName;
@property(nonatomic,retain)NSString *contactGuid;
@property (retain, nonatomic) IBOutlet UILabel *designation;
@property (retain, nonatomic) IBOutlet UIButton *primaryCallButton;
@property (retain, nonatomic) IBOutlet UILabel *primaryNumber;
@property (retain, nonatomic) IBOutlet UIButton *secondaryCallButton;
@property (retain, nonatomic) IBOutlet UILabel *secondaryNumber;
- (IBAction)callToTheContact:(id)sender;

@end
