//
//  ContactViewController.m
//  ICommConnect
//
//  Created by Ravikanth on 9/3/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "ContactViewController.h"
#import "ICommConnectAppDelegate.h"
#import "RestApi+ContactDetails.h"
#import "RestApi+SubsriberInfo.h"
@interface ContactViewController ()
{
    NSMutableDictionary *receivedDictionary;
    __block NSData *imageData;
}
@end
@implementation ContactViewController
@synthesize contactGuid;
- (void)viewDidLoad {
    [super viewDidLoad];
     [self loadViewbackgroundColor];
     [self setNavigationBarSettings];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.contactName setFont:titleFont];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshApp"]) {
         [[self navigationController] setNavigationBarHidden:YES animated:YES];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}
//app refreshing whenever there is no connection

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    if([self getObjectForKey:@"Connection"]){
        if ([[self getObjectForKey:@"Connection"] isEqualToString:@"Failed"]) {
            [self viewWillAppear:YES];
        }
    }
    
}
- (void)viewWillAppear:(BOOL)animated{
 [self getContactDetails:contactGuid];// calling a Web api to get contact details
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
}
-(void)getContactDetails:(NSString*)contactGUID{
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    [[RestApi alloc] getContactDetails:self forContact:contactGUID];
}
//handling the result of web api(contact details)
-(void)handleContactDetailsResult:(NSMutableDictionary *)responseDictionary{
    if(responseDictionary.count>0){
    receivedDictionary=[responseDictionary mutableCopy];
        NSString *subscriptionID=[responseDictionary objectForKey:@"SubscriptionGUID"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshApp"]) {
            [[ICommConnectAppDelegate sharedInstance] clearDefaults];
            [self setLocalObject:subscriptionID forKey:@"SubscriptionGUId"];
            [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
            
            [[RestApi alloc] getSubScriberInfo:self forSubscriberId:subscriptionID];
        }
    self.title=[responseDictionary objectForKey:@"DepartmentName"];
    self.contactName.text=[responseDictionary objectForKey:@"ContactName"];
    self.designation.text=[responseDictionary objectForKey:@"ContactDesignation"];
    NSArray *contactsArray=[responseDictionary objectForKey:@"ContactNumbers"];
    self.primaryNumber.text=[NSString stringWithFormat:@"%@ (%@)",[[contactsArray objectAtIndex:0] objectForKey:@"ContactNo"],[[contactsArray objectAtIndex:0] objectForKey:@"PhoneType"]];
    if([contactsArray count]>1){
        self.secondaryNumber.text=[NSString stringWithFormat:@"%@ (%@)",[[contactsArray objectAtIndex:1] objectForKey:@"ContactNo"],[[contactsArray objectAtIndex:1] objectForKey:@"PhoneType"]];
    }else{
        self.secondaryNumber.hidden=YES;
        self.secondaryCallButton.hidden=YES;
    }
    //Checking CanCall flag to call
    if (![[responseDictionary objectForKey:@"CanCall"] boolValue]) {
        self.primaryCallButton.hidden=YES;
        self.secondaryCallButton.hidden=YES;
    }
    //Checking ShowContact flag to display contact details
    if (![[responseDictionary objectForKey:@"ShowContact"] boolValue]) {
        self.primaryNumber.text=@"(Primary)";
        self.secondaryNumber.text=@"(Secondary)";
    }
    
    //loading contact image on background
    NSString *contactImageURL=[responseDictionary objectForKey:@"ContactImage"];
        if (![contactImageURL isKindOfClass:[NSNull class]]) {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
    dispatch_async(queue, ^{
        imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:contactImageURL]];
        dispatch_sync(dispatch_get_main_queue(), ^{
            @try {
                if(imageData !=(NSData *)nil){
                    self.contactImageView.image=[UIImage imageWithData:imageData];
                }
            }
            @catch (NSException *exception) {
                  self.contactImageView.image=[UIImage imageNamed:@"NoImage.png"];
                
            }
           
        });
    });
    }
 self.contactImageView.contentMode=UIViewContentModeScaleAspectFit;
    }else{
        self.primaryCallButton.hidden=YES;
        self.secondaryCallButton.hidden=YES;
        [self showAlertViewWithTitle:@"Message" withMessage:@"Sorry,we couldn't find the contact details."];
    }
     [SVProgressHUD dismiss];
}
//handling subscriber api result
-(void)handleSubscriberInfoResult:(NSMutableDictionary *)responseDictionary{
    if(![responseDictionary isKindOfClass:[NSNull class]]&&[responseDictionary count]>0)
    {
        [self setLocalObject:[responseDictionary objectForKey:@"DirectoryiconColor"] forKey:@"DirectoryiconColor"];
        [self setLocalObject:[responseDictionary valueForKeyPath:@"DirectoryBackgroundColor"] forKey:@"DirectoryBackgroundColor"];
        [self setLocalObject:[NSString stringWithFormat:@"%@",[responseDictionary valueForKeyPath:@"SubID"]] forKey:@"SubID"];
        [self loadViewbackgroundColor];
        if (![[responseDictionary objectForKey:@"IsStarted"] boolValue] || [[responseDictionary objectForKey:@"IsExpired"] boolValue] || [[responseDictionary valueForKeyPath:@"Status"] isEqualToString:@"Delinquent"]) {
            self.dataView.hidden=YES;
            [self showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"%@%@",Delinquent_Message,[responseDictionary valueForKeyPath:@"DefaultContact"]]];
            return;
        }
    }
   [SVProgressHUD dismiss];
}

//connecting to the contact
- (IBAction)callToTheContact:(id)sender {
    if (receivedDictionary.count>0) {
    [(UIButton*)sender setHighlighted:YES];
    int tag = (int)((UIButton*)sender).tag;
    [self setLocalObject:[receivedDictionary objectForKey:@"ContactGuid"] forKey:@"contactGuid"];
    AudioCallViewController *presentController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioView"];
    presentController.contactIndex=tag;
    presentController.contactDictionary=receivedDictionary;
        @try {
            if(imageData !=(NSData *)nil){
                presentController.imageData=imageData;
            }
        }
        @catch (NSException *exception) {
            
        }

    presentController.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:presentController animated:YES completion:nil];
    }
}


@end
