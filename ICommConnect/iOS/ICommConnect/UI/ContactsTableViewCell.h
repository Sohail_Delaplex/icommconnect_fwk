//
//  ContactsTableViewCell.h
//  ICommConnect
//
//  Created by Ravikanth on 8/24/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *seperationLine;
@property (retain, nonatomic) IBOutlet UIImageView *contactImageView;
@property (retain, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *designationLabel;
@property (retain, nonatomic) IBOutlet UILabel *primaryContactLabel;
@property (retain, nonatomic) IBOutlet UILabel *secondaryContactLabel;
@property (retain, nonatomic) IBOutlet UIButton *primaryContactButton;
@property (retain, nonatomic) IBOutlet UIButton *secondaryContactButton;
- (IBAction)callToTheContact:(id)sender;
@end
