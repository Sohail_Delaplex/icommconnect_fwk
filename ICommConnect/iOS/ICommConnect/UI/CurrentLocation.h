//
//  CurrentLocation.h
//  Tow_4.5x
//
//  Created by Ravikanth Muthavarapu on 14/09/14.
//  Copyright (c) 2014 Prime. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@interface CurrentLocation : NSObject<CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
}
+ (CurrentLocation *)sharedInstance;
-(NSString*)findCurrentLocation;
@end
