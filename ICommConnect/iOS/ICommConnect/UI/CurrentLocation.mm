//
//  CurrentLocation.m
//  Tow_4.5x
//
//  Created by Ravikanth Muthavarapu on 14/09/14.
//  Copyright (c) 2014 Prime. All rights reserved.
//

#import "CurrentLocation.h"
@implementation CurrentLocation
+ (CurrentLocation *)sharedInstance {
    static dispatch_once_t onceToken;
    static CurrentLocation *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[CurrentLocation alloc] init];
    });
    return instance;
};
- (id)init {
    self = [super init];
    if (self) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.delegate = self;
        NSString * osVersion = [[UIDevice currentDevice] systemVersion];
        if ([osVersion floatValue]>= 8.0 ) {
            [locationManager requestWhenInUseAuthorization]; //Requests permission to use location services whenever the app is running.
            // [_CLLocationManager requestWhenInUseAuthorization]; //Requests permission to use location services while the app is in the foreground.
        }

       // countDownTimer=[[NSTimer alloc] init];
        
    }
    return self;
}
-(NSString*)findCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    NSString * osVersion = [[UIDevice currentDevice] systemVersion];
    if ([osVersion floatValue]>= 8.0 ) {
        [locationManager requestWhenInUseAuthorization]; //Requests permission to use location services whenever the app is running.
        // [_CLLocationManager requestWhenInUseAuthorization]; //Requests permission to use location services while the app is in the foreground.
    }

    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
    }
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    NSString *str=[[NSString alloc] initWithFormat:@"%f~%f",coordinate.latitude,coordinate.longitude];
   // NSLog(@"ravi==%@",str);
    
    //  NSLog(@"%@",str);
    return str;
}

@end
