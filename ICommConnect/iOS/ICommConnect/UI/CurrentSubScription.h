//
//  CurrentSubScription.h
//  ICommConnect
//
//  Created by Ravikanth on 3/22/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentSubScription : NSObject
@property (nonatomic, readwrite) BOOL allowCall;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *defaultContactNumber;
+ (CurrentSubScription*)sharedInstance;
@end
