//
//  CurrentSubScription.m
//  ICommConnect
//
//  Created by Ravikanth on 3/22/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "CurrentSubScription.h"
static CurrentSubScription* inst = nil;
@implementation CurrentSubScription
@synthesize allowCall;
@synthesize status;
@synthesize defaultContactNumber;
-(id)init {
    if(self=[super init]) {
        self.allowCall=NO;
        self.status=@"";
        self.defaultContactNumber=@"";
    }
    return self;
}
+(CurrentSubScription*)sharedInstance {
    if (!inst) inst = [[CurrentSubScription alloc] init];
        return inst;
}
@end
