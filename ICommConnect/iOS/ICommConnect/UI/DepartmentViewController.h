//
//  DepartmentViewController.h
//  ICommConnect
//
//  Created by Ravikanth on 8/24/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "BaseViewController.h"
#import "SubDepartmentsDelegate.h"
#import "ContactsDelegate.h"
@interface DepartmentViewController :BaseViewController<SubDepartmentsDelegate,ContactsDelegate>
@property (retain, nonatomic) IBOutlet UITableView *departmentTableView;
@property(nonatomic,retain)NSString *departmentID;
@end
