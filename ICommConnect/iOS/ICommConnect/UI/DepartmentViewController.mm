//
//  DepartmentViewController.m
//  ICommConnect
//
//  Created by Ravikanth on 8/24/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "DepartmentViewController.h"
#import "ContactsTableViewCell.h"
#import "DepartmentTableViewCell.h"
#import "ICommConnectAppDelegate.h"
#import "RestApi+SubDepartments.h"
#import "RestApi+Contacts.h"
@interface DepartmentViewController (){
    UIView *departmentsGroupView;
    float cellHeight;
    NSMutableDictionary *subDepartmentResponseData;
    NSMutableDictionary *sortedDictionary;
    NSMutableArray *departmentNames;
    NSMutableArray *contactsArray;
    NSMutableDictionary *imagecache;
    NSMutableArray *departmentsCellHieghtsArray;
}
@end
@implementation DepartmentViewController
@synthesize departmentID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewbackgroundColor];
    [self setNavigationBarSettings];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}
//app refreshing whenever there is no connection

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    if([self getObjectForKey:@"Connection"]){
        if ([[self getObjectForKey:@"Connection"] isEqualToString:@"Failed"]) {
            [self viewWillAppear:YES];
        }
    }
    
}
- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    [self loadDepartmentDetails];
    imagecache=[[NSMutableDictionary alloc]init];// for images loading on cells
    departmentsCellHieghtsArray=[[NSMutableArray alloc ] init];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number;
    if(section==0){
        number= [departmentNames count];
    }else{
        number= [contactsArray count];
    }
    return number;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float _cellHeight;
    if (indexPath.section==0 && departmentsCellHieghtsArray.count>0) {
        _cellHeight=[[departmentsCellHieghtsArray objectAtIndex:indexPath.row] integerValue];
    }if (indexPath.section==1){
        if ([[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactNumbers"] count]>1) {
            _cellHeight=140;
        }else{
            _cellHeight=110;
        }
    }
    return _cellHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactsTableViewCell *contactsCell;
    DepartmentTableViewCell *departmentCell;
    if(indexPath.section==0){
        //loading department table view cell
        NSString *cellIdentifier = @"DepartmentTableViewCell";
        departmentCell = (DepartmentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (departmentCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:nil options:nil];
            departmentCell = (DepartmentTableViewCell*)[nib objectAtIndex:0];
        }
        departmentsGroupView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
        cellHeight=[self designCustomView:departmentsGroupView withDepartment:[sortedDictionary objectForKey:[departmentNames objectAtIndex:indexPath.row]] title:[departmentNames objectAtIndex:indexPath.row]];
        [departmentsCellHieghtsArray addObject:[NSString stringWithFormat:@"%f",cellHeight]];
        [departmentCell.contentView addSubview:departmentsGroupView];
        
        departmentCell.backgroundColor=[UIColor clearColor];
        
    }else{
        
        //loading contact table view cell
        NSString *cellIdentifier = @"ContactsTableViewCell";
        contactsCell = (ContactsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (contactsCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            contactsCell = [nib objectAtIndex:0];
        }
        if ([departmentNames count]<1&& indexPath.row==0) {
            contactsCell.seperationLine.hidden=YES;
        }
        contactsCell.contactNameLabel.text=[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactName"];
        contactsCell.contactNameLabel.font=detailedFont;
        contactsCell.designationLabel.text=[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactDesignation"];
        contactsCell.designationLabel.font=detailedFont;
        if ([[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactNumbers"] count]>0) {
            NSString *primarycontactType=[[[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactNumbers"] objectAtIndex:0] objectForKey:@"PhoneType"];
            NSString *secondaryContactType;
            contactsCell.primaryContactLabel.text=[NSString stringWithFormat:@"%@ (%@)",[[[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactNumbers"] objectAtIndex:0] objectForKey:@"ContactNo"],primarycontactType];
            contactsCell.primaryContactLabel.font=detailedFont;
            [contactsCell.primaryContactButton addTarget:self action:@selector(callToThePrimaryContact:) forControlEvents:UIControlEventTouchUpInside];
            contactsCell.primaryContactButton.tag=indexPath.row;
            //checking whether there is a secondary contact or not
            if([[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactNumbers"] count]>1){
                secondaryContactType=[[[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactNumbers"] objectAtIndex:1] objectForKey:@"PhoneType"];
                contactsCell.secondaryContactLabel.font=detailedFont;
                contactsCell.secondaryContactLabel.text=[NSString stringWithFormat:@"%@ (%@)",[[[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactNumbers"] objectAtIndex:1] objectForKey:@"ContactNo"],secondaryContactType];
                [contactsCell.secondaryContactButton addTarget:self action:@selector(callToTheSecondaryContact:) forControlEvents:UIControlEventTouchUpInside];
                contactsCell.secondaryContactButton.tag=indexPath.row;
                contactsCell.secondaryContactButton.hidden=NO;
            }else{
                contactsCell.secondaryContactLabel.hidden=YES;
                contactsCell.secondaryContactButton.hidden=YES;
            }
            //checking CanCall, if CanCall is true then only we need to allow user to call.
            if (![[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"CanCall"] boolValue]) {
                contactsCell.secondaryContactButton.hidden=YES;
                contactsCell.primaryContactButton.hidden=YES;
            }
            //checking ShowContact, if Show Contact is true then only we need to show contact details.
            if (![[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ShowContact"] boolValue]) {
                contactsCell.primaryContactLabel.text=[NSString stringWithFormat:@"%@ (Primary)",primarycontactType];
                contactsCell.secondaryContactLabel.text=[NSString stringWithFormat:@"%@ (Secondary)",secondaryContactType];
            }
            
        }else{
            contactsCell.secondaryContactButton.hidden=YES;
            contactsCell.primaryContactButton.hidden=YES;
        }
        contactsCell.backgroundColor=[UIColor clearColor];
        //loading contact images on background.
        if([imagecache objectForKey:[NSString stringWithFormat:@"%d", (int)indexPath.row]] == NULL){
            NSString *postpictureUrl=[[contactsArray objectAtIndex:indexPath.row] objectForKey:@"ContactImage"];
            if(![postpictureUrl isKindOfClass:[NSNull class]]){
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                dispatch_async(queue, ^{
                    NSData   *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:postpictureUrl]];
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        if(![data isKindOfClass:[NSNull class]]){
                            @try {
                                contactsCell.contactImageView.image = [UIImage imageWithData:data];
                                [imagecache setObject:data forKey:[NSString stringWithFormat:@"%d", (int)indexPath.row]];
                            }
                            @catch (NSException *exception) {
                               contactsCell.contactImageView.image=[UIImage imageNamed:@"Tile_default.png"];
                            }
                      
                        }
                    });
                });
            }
        }else{
            @try {
                   contactsCell.contactImageView.image = [UIImage imageWithData:[imagecache objectForKey:[NSString stringWithFormat:@"%d", (int)indexPath.row]]];
            }
            @catch (NSException *exception) {
            }

        
        }
    }
    return (indexPath.section==0)?departmentCell:contactsCell;
}
- (void)orientationChanged:(NSNotification *)notification{
    UIInterfaceOrientation orientation = (UIInterfaceOrientation)[[UIDevice currentDevice] orientation];
    if(orientation == UIInterfaceOrientationPortrait||UIInterfaceOrientationLandscapeRight||UIInterfaceOrientationLandscapeLeft||UIInterfaceOrientationPortraitUpsideDown)
    {
        [departmentsCellHieghtsArray removeAllObjects];
        [self.departmentTableView reloadData];
    }
    
}
//calling Web Api to get subdepartments of a department
-(void)loadDepartmentDetails{
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    [[RestApi alloc] getSubDepartments:self forDepartment:departmentID];
}
//Handling result of sub departments web api.
-(void)handleSubDepartmentsResult:(NSMutableDictionary *)responseDictionary{
    if ([responseDictionary count]>0) {
        subDepartmentResponseData=[responseDictionary mutableCopy];
        self.title=[responseDictionary objectForKey:@"Name"];
        [self setSubDepartments];
    }
    //calling Web Api to get contact details of department.
    [[RestApi alloc] getContacts:self forDepartment:departmentID];
}

//Handling result of sub departments contacts web api.
-(void)handleContactsResult:(NSMutableDictionary *)responseDictionary{
    if ([responseDictionary count]>0) {
        contactsArray=[responseDictionary mutableCopy];
        [self loadtable];
        
    }
    [SVProgressHUD dismiss];
}

//Sorting results of deprtments objects and reloading table.
-(void)setSubDepartments{
    if ([[subDepartmentResponseData objectForKey:@"Departments"] count]>0) {
        
        NSMutableDictionary *departmentsDictionary=[subDepartmentResponseData objectForKey:@"Departments"];
        sortedDictionary = [self groupTheDepartmentsByGroupName:departmentsDictionary];
        
        NSArray *keys = [sortedDictionary allKeys];
        departmentNames = [keys mutableCopy];
        departmentNames=[self sortTheArray:departmentNames];
    }
    [self loadtable];
    
}
-(void)loadtable{
    [self.departmentTableView reloadData];
}
//calling to the primary contact.
-(void)callToThePrimaryContact:(UIButton *)sender{
    //UIButton *btn=(UIButton*)sender;
    int tag=(unsigned int)sender.tag;
    if([imagecache objectForKey:[NSString stringWithFormat:@"%d",tag]]){
        [self connectCallToTheNumber:[contactsArray objectAtIndex:tag] withIndex:0 andwithImage:[imagecache objectForKey:[NSString stringWithFormat:@"%d",tag]]];
    }else{
        [self connectCallToTheNumber:[contactsArray objectAtIndex:tag] withIndex:0 andwithImage:nil];
    }
}
//calling to the secondary contact.
-(void)callToTheSecondaryContact:(UIButton *)sender{
    //  UIButton *btn=(UIButton*)sender;
    int tag=(unsigned int)sender.tag;
    if([imagecache objectForKey:[NSString stringWithFormat:@"%d",tag]]){
        [self connectCallToTheNumber:[contactsArray objectAtIndex:tag] withIndex:1 andwithImage:[imagecache objectForKey:[NSString stringWithFormat:@"%d",tag]]];
    }else{
        [self connectCallToTheNumber:[contactsArray objectAtIndex:tag] withIndex:1 andwithImage:nil];
    }
}
//sending data to audio call view to make call.
-(void)connectCallToTheNumber:(NSDictionary*)contactDictionary withIndex:(int)index andwithImage:(NSData*)imageData{
    [self setLocalObject:[contactDictionary objectForKey:@"ContactGuid"] forKey:@"contactGuid"];
    AudioCallViewController *presentController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioView"];
    presentController.contactIndex=index;
    presentController.contactDictionary=contactDictionary;
    presentController.imageData=imageData;
    presentController.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:presentController animated:YES completion:nil];
}
-(void)btnAction:(id)sender{
    UIButton *btn=(UIButton*)sender;
    for (id obj in [subDepartmentResponseData objectForKey:@"Departments"]) {
        id keyValue = [obj valueForKey:@"ShortName"];
        
        if([keyValue isEqualToString:btn.titleLabel.text]){
            DepartmentViewController *pushToController = [self.storyboard instantiateViewControllerWithIdentifier:@"DepartmentView"];
            pushToController.departmentID=[obj objectForKey:@"Guid"];
            [self.navigationController pushViewController:pushToController animated:YES];
        }
    }
}
- (void)dealloc {
}
@end
