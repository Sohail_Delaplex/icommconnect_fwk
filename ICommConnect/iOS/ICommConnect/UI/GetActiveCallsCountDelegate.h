//
//  GetActiveCallsCountDelegate.h
//  ICommConnect
//
//  Created by Ravikanth on 2/9/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GetActiveCallsCountDelegate <NSObject>
- (void) handleActiveCallsResult:(NSDictionary*)responseDictionary;
@end
