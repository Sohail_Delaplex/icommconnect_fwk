//
//  GetSubscriprionsDelegate.h
//  ICommConnect
//
//  Created by Ravikanth on 12/2/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GetSubscriprionsDelegate <NSObject>
- (void) handlegetSubscriptionsResult:(NSArray*)responseArray;
@end
