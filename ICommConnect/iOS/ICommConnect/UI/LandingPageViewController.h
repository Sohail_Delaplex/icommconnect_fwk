//
//  LandingPageViewController.h
//  ICommConnect
//
//  Created by Ravikanth on 12/2/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "BaseViewController.h"
#import "GetSubscriprionsDelegate.h"
@interface LandingPageViewController : BaseViewController<GetSubscriprionsDelegate>
@property (retain, nonatomic) IBOutlet UIView *subscribersView;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *subScriptionsViewHeightConstraint;

@end
