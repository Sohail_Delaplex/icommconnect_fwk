//
//  LandingPageViewController.m
//  ICommConnect
//
//  Created by Ravikanth on 12/2/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "LandingPageViewController.h"
#import "BaseViewController.h"
#import "RestApi+getAllSubscriptions.h"
#import "SubScriptionViewController.h"
#import "ICommConnectAppDelegate.h"
#import "CurrentLocation.h"
@interface LandingPageViewController ()
{
    NSArray *subscriptionsArray;
}
@end

@implementation LandingPageViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"loc value:%@",[[CurrentLocation sharedInstance] findCurrentLocation]);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}
//app refreshing whenever there is no connection

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    if([self getObjectForKey:@"Connection"]){
        if ([[self getObjectForKey:@"Connection"] isEqualToString:@"Failed"]) {
        [self viewWillAppear:YES];
        }
    }
    
}
-(void)loadLandingPageData{

}
//handling the view when orientation is changed
- (void)orientationChanged:(NSNotification *)notification{
    UIInterfaceOrientation orientation = (UIInterfaceOrientation)[[UIDevice currentDevice] orientation];
    if(orientation == UIInterfaceOrientationPortrait||UIInterfaceOrientationLandscapeRight||UIInterfaceOrientationLandscapeLeft||UIInterfaceOrientationPortraitUpsideDown)
    {
        [self clearTheDirectories];
        [self handlegetSubscriptionsResult:subscriptionsArray];
    }
    
}
//This method is to clear all the views
-(void)clearTheDirectories{
    for(UIView* view in self.subscribersView.subviews)
    {
        [view removeFromSuperview];
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [self clearTheDirectories];//clearing the all views before loading new ones.
    [self getAllSubscriptions];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
-(void)getAllSubscriptions{
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    [[RestApi alloc] getAllSubscriptions:self];
}
-(void)handlegetSubscriptionsResult:(NSArray *)responseArray{
    [SVProgressHUD dismiss];
    subscriptionsArray=[self filterSubScriptionsArray:responseArray];
    // NSLog(@"responseDictionary:%@",responseArray);
    float height=[self designCustomView:self.subscribersView withDepartment:subscriptionsArray title:@"##@"];
    [self.subscribersView removeConstraint:_subScriptionsViewHeightConstraint];
    _subScriptionsViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.subscribersView                                                             attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:height];
    [self.subscribersView addConstraint:_subScriptionsViewHeightConstraint];
}
-(void)btnAction:(UIButton *)sender{
    // NSMutableDictionary *dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"imageChache"];
    [[ICommConnectAppDelegate sharedInstance] clearDefaults];
    [self setLocalObject:[[subscriptionsArray objectAtIndex:sender.tag] objectForKey:@"Guid"] forKey:@"SubscriptionGUId"];
    // [self setLocalObject:dict forKey:@"imageChache"];
    SubScriptionViewController *pushToController = [self.storyboard instantiateViewControllerWithIdentifier:@"SubscriptionView"];
    pushToController.isSearchEnabled=[[[subscriptionsArray objectAtIndex:sender.tag] objectForKey:@"ShowSearch"] boolValue];
    [self.navigationController pushViewController:pushToController animated:YES];
}
-(id)filterSubScriptionsArray:(NSArray*)inputArray{
    NSMutableArray *activeSubScriptionsArray=[[NSMutableArray alloc]  init];
    for(id obj in inputArray){
        if ([[obj objectForKey:@"IsActive"] boolValue]&& [[obj objectForKey:@"IsStarted"] boolValue]&&![[obj objectForKey:@"IsExpired"] boolValue]) {
            [activeSubScriptionsArray addObject:obj];
        }
    }
    return activeSubScriptionsArray;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
