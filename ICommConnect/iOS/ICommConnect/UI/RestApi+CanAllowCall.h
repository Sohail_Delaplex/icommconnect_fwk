//
//  RestApi+CanAllowCall.h
//  ICommConnect
//
//  Created by Ravikanth on 2/19/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "CanAllowCall.h"
@interface RestApi (CanAllowCall)
- (void) getIPStatusToAllowCall:(id <CanAllowCall,RestDelegate>) delegate forSubscriptionGuId:(NSString*)subscriptionGuid withIPAddres:(NSString*)ipAddress;
@end
