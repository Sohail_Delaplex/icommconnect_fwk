//
//  RestApi+CanAllowCall.m
//  ICommConnect
//
//  Created by Ravikanth on 2/19/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi+CanAllowCall.h"

@implementation RestApi (CanAllowCall)
- (void) getIPStatusToAllowCall:(id <CanAllowCall,RestDelegate>) delegate forSubscriptionGuId:(NSString*)subscriptionGuid withIPAddres:(NSString*)ipAddress{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/CanAllowCall?subscriptionId=%@&IP=%@",subscriptionGuid,ipAddress];
    [self executeRequest:endpoint delegate:delegate success:@selector(canAllowCallSuccessCallBack:) failure:nil];
}
/*Success CallBack*/
- (void) canAllowCallSuccessCallBack:(RestApi*)object{
    id<CanAllowCall> delegatee=object.delegated;
     NSLog(@"res:%@",[self responseDictionary]);
    [delegatee handleCanAllowCallResult:[self responseDictionary]];
}

@end
