//
//  RestApi+ContactDetails.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "ContactDetailsDelegate.h"
@interface RestApi (ContactDetails)
- (void) getContactDetails:(id <ContactDetailsDelegate,RestDelegate>) delegate forContact:(NSString*)contactGuid;
@end
