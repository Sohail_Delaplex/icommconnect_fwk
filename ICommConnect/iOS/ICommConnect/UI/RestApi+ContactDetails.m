//
//  RestApi+ContactDetails.m
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+ContactDetails.h"

@implementation RestApi (ContactDetails)
/*Invoking GetContactDetails Api*/
- (void) getContactDetails:(id <ContactDetailsDelegate,RestDelegate>) delegate forContact:(NSString*)contactGuid{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetContact?contactGuId=%@",contactGuid];
    [self executeRequest:endpoint delegate:delegate success:@selector(contactDetailsApiSuccess:) failure:nil];
}
/*Success CallBack*/
- (void) contactDetailsApiSuccess:(RestApi*)object{
    id<ContactDetailsDelegate> delegatee=object.delegated;
    NSMutableDictionary *response = [self responseDictionary];
    [delegatee handleContactDetailsResult:response];
}
@end
