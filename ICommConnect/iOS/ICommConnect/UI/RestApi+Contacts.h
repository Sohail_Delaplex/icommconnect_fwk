//
//  RestApi+Contacts.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "ContactsDelegate.h"
@interface RestApi (Contacts)
- (void) getContacts:(id <ContactsDelegate,RestDelegate>) delegate forDepartment:(NSString*)departmentGuid;
@end
