//
//  RestApi+Contacts.m
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+Contacts.h"
@implementation RestApi (Contacts)
/*Sending GetContacts EndPoint to Make URL Request*/
- (void) getContacts:(id <ContactsDelegate,RestDelegate>) delegate forDepartment:(NSString*)departmentGuid{
NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetContacts?departmentGuid=%@",departmentGuid];
[self executeRequest:endpoint delegate:delegate success:@selector(contactsApiSuccess:) failure:nil];
}
/*Success CallBack*/
- (void) contactsApiSuccess:(RestApi*)object{
    id<ContactsDelegate> delegatee=object.delegated;
    NSMutableDictionary *response = [self responseDictionary];
    [delegatee handleContactsResult:response];
}
@end
