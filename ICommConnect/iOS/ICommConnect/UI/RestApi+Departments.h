//
//  RestApi+Departments.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "DepartmentsDelegate.h"
@interface RestApi (Departments)
- (void) getSubScriberDepartments:(id <DepartmentsDelegate,RestDelegate>) delegate forSubscriberId:(NSString*)subscriberID;
@end
