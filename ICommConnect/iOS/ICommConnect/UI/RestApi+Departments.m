//
//  RestApi+Departments.m
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+Departments.h"

@implementation RestApi (Departments)
/*Invoking SubscriberDepartments Data*/
- (void) getSubScriberDepartments:(id <DepartmentsDelegate,RestDelegate>) delegate forSubscriberId:(NSString*)subscriberID{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetDepartments?subscriptionGuId=%@",subscriberID];
    [self executeRequest:endpoint delegate:delegate success:@selector(departmentsApiSuccess:) failure:nil];
}
/*Success CallBack Method*/
- (void) departmentsApiSuccess:(RestApi*)object{
    id<DepartmentsDelegate> delegatee=object.delegated;
    NSMutableDictionary *response = [self responseDictionary];
    [delegatee handleDepartmentsResult:response];
}
@end

