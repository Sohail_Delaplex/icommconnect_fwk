//
//  RestApi+GetActiveCallsCount.h
//  ICommConnect
//
//  Created by Ravikanth on 2/9/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "GetActiveCallsCountDelegate.h"
@interface RestApi (GetActiveCallsCount)
- (void) getActiveCallsCount:(id <GetActiveCallsCountDelegate,RestDelegate>) delegate forSubscriptionGuId:(NSString*)subscriptionGuid;
@end
