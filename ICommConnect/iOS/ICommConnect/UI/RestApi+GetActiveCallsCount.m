//
//  RestApi+GetActiveCallsCount.m
//  ICommConnect
//
//  Created by Ravikanth on 2/9/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi+GetActiveCallsCount.h"

@implementation RestApi (GetActiveCallsCount)
- (void) getActiveCallsCount:(id <GetActiveCallsCountDelegate,RestDelegate>) delegate forSubscriptionGuId:(NSString*)subscriptionGuid{

    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetActiveCallsCount?subscriptionId=%@",subscriptionGuid];
    [self executeRequest:endpoint delegate:delegate success:@selector(activeCallsCountSuccess:) failure:nil];
//    NSError* error;
//    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:[self synchronousRequestUrl:endpoint]
//                                                         options:kNilOptions
//                                                           error:&error];
//    
//      [delegate handleActiveCallsResult:json];
    
    
}
/*Success CallBack*/
- (void) activeCallsCountSuccess:(RestApi*)object{
    id<GetActiveCallsCountDelegate> delegatee=object.delegated;
    NSMutableDictionary *response = [self responseDictionary];
    NSLog(@"response:%@",response);
    [delegatee handleActiveCallsResult:response];
}

@end
