//
//  RestApi+GetMyIP.h
//  ICommConnect
//
//  Created by Ravikanth on 2/19/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "GetMyIPDelegate.h"
@interface RestApi (GetMyIP)
- (void) getDevicePublicIP:(id <GetMyIPDelegate,RestDelegate>) delegate;
@end
