//
//  RestApi+GetMyIP.m
//  ICommConnect
//
//  Created by Ravikanth on 2/19/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi+GetMyIP.h"

@implementation RestApi (GetMyIP)
- (void) getDevicePublicIP:(id <GetMyIPDelegate,RestDelegate>) delegate{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetMYIp"];
    [self executeRequest:endpoint delegate:delegate success:@selector(getDeviceIPSuccessCallBack:) failure:nil];
}
/*Success CallBack*/
- (void) getDeviceIPSuccessCallBack:(RestApi*)object{
    id<GetMyIPDelegate> delegatee=object.delegated;
    NSLog(@"res:%@",[self responseDictionary]);
    [delegatee handleGetMyIPResult:[self responseDictionary]];
}
@end
