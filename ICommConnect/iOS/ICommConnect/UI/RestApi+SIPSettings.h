//
//  RestApi+SIPSettings.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "SipDelegate.h"
@interface RestApi (SIPSettings)
- (void) getSIPSettings:(id <SipDelegate,RestDelegate>) delegate withSubscriptionGuId:(NSString*)subscriptionGuid;
@end
