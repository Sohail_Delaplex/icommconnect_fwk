//
//  RestApi+SIPSettings.m
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+SIPSettings.h"

@implementation RestApi (SIPSettings)
/*Sending SIPSettings Web Api to Establish URL Connection*/
- (void) getSIPSettings:(id <SipDelegate,RestDelegate>) delegate withSubscriptionGuId:(NSString*)subscriptionGuId{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetSipSettings?subscriptionGuId=%@",subscriptionGuId];
    [self executeRequest:endpoint delegate:delegate success:@selector(sipDetailsApiSuccess:) failure:nil];
}
/*Success CallBack*/
- (void) sipDetailsApiSuccess:(RestApi*)object{
    id<SipDelegate> delegatee=object.delegated;
    NSMutableDictionary *response = [self responseDictionary];
    NSLog(@"response:%@",response);
    [delegatee handleSIPResult:response];
}
@end
