//
//  RestApi+Search.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "SeacrhDelegate.h"
@interface RestApi (Search)
- (void) getSearchResults:(id <SeacrhDelegate,RestDelegate>) delegate withTerm:(NSString*)searchTerm withSubscriptionGuid:(NSString*)subscriptionGuid;
@end
