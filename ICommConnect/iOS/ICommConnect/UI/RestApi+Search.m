//
//  RestApi+Search.m
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+Search.h"

@implementation RestApi (Search)
/*Invoking SeacrchResults Api*/
- (void) getSearchResults:(id <SeacrhDelegate,RestDelegate>) delegate withTerm:(NSString*)searchTerm withSubscriptionGuid:(NSString*)subscriptionGuid{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/SearchResult?term=%@&subscriptionguid=%@",searchTerm,subscriptionGuid];
    [delegate handleSearchResult:[self synchronousRequestUrl:endpoint]];
}
@end
