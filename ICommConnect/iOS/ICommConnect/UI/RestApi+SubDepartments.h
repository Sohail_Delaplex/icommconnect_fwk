//
//  RestApi+SubDepartments.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "SubDepartmentsDelegate.h"
@interface RestApi (SubDepartments)
- (void) getSubDepartments:(id <SubDepartmentsDelegate,RestDelegate>) delegate forDepartment:(NSString*)departmentGuid;
@end
