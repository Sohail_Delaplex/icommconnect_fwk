//
//  RestApi+SubDepartments.m
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+SubDepartments.h"

@implementation RestApi (SubDepartments)
/*Invoking SubDepartement Api*/
- (void) getSubDepartments:(id <SubDepartmentsDelegate,RestDelegate>) delegate forDepartment:(NSString*)departmentGuid{
NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetDepartment?departmentGuId=%@",departmentGuid];
[self executeRequest:endpoint delegate:delegate success:@selector(subDepartmentApiSuccess:) failure:nil];
}
/*Success CallBack*/
- (void) subDepartmentApiSuccess:(RestApi*)object{
    id<SubDepartmentsDelegate> delegatee=object.delegated;
    NSMutableDictionary *response = [self responseDictionary];
    [delegatee handleSubDepartmentsResult:response];
}

@end
