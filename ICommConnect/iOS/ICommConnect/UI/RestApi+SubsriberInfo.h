//
//  RestApi+SubsriberInfo.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "SubscriberInfoDelegate.h"
@interface RestApi (SubsriberInfo)
- (void) getSubScriberInfo:(id <SubscriberInfoDelegate,RestDelegate>) delegate forSubscriberId:(NSString*)subscriberID;
@end
