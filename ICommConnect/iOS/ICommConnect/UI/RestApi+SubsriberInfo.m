//
//  RestApi+SubsriberInfo.m
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+SubsriberInfo.h"

@implementation RestApi (SubsriberInfo)
/*Invoking SubscriberInfo data*/
- (void) getSubScriberInfo:(id <SubscriberInfoDelegate,RestDelegate>) delegate forSubscriberId:(NSString*)subscriberID{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetSubscription?subscriptionGuId=%@",subscriberID];
    [self executeRequest:endpoint delegate:delegate success:@selector(subsciberInfoFinished:) failure:nil];
}
/*Success CallBack Method*/
- (void) subsciberInfoFinished:(RestApi*)object{
    id<SubscriberInfoDelegate> delegatee=object.delegated;
    NSMutableDictionary *response = [self responseDictionary];
    [delegatee handleSubscriberInfoResult:response];
}
@end
