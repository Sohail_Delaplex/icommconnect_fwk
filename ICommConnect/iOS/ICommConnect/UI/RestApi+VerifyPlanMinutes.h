//
//  RestApi+VerifyPlanMinutes.h
//  ICommConnect
//
//  Created by Ravikanth on 3/9/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
@interface RestApi (VerifyPlanMinutes)
- (void) callJobVerifyPlanMinutes:(id <RestDelegate>) delegate forSubscriberId:(NSString*)subscriberID;
@end
