//
//  RestApi+VerifyPlanMinutes.m
//  ICommConnect
//
//  Created by Ravikanth on 3/9/16.
//  Copyright (c) 2016 Doubango Telecom. All rights reserved.
//

#import "RestApi+VerifyPlanMinutes.h"
#import "Constants.h"
@implementation RestApi (VerifyPlanMinutes)
- (void) callJobVerifyPlanMinutes:(id <RestDelegate>) delegate forSubscriberId:(NSString*)subscriberID{
    NSString *endpoint = [NSString stringWithFormat:@"%@/Job/VerifyPlanMinutes?clientSubscriptionGuid=%@",kBaseURL,subscriberID];
  
    NSURL *URL = [NSURL URLWithString:[endpoint stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    @try {
        NSURLRequest *requestURL = [[NSURLRequest alloc] initWithURL:URL];
        [NSURLConnection sendAsynchronousRequest:requestURL
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             //When Json request complite then call this block
//             NSLog(@"Json Call Done Perform Your Action with data");
//             NSLog(@"Reply is:%@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
         }];

    }
    @catch (NSException *exception) {
         NSLog(@"exception:");
    }
}
@end
