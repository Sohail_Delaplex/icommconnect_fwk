//
//  RestApi+getAllSubscriptions.h
//  ICommConnect
//
//  Created by Ravikanth on 12/2/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi.h"
#import "GetSubscriprionsDelegate.h"
@interface RestApi (getAllSubscriptions)
- (void) getAllSubscriptions:(id <GetSubscriprionsDelegate,RestDelegate>) delegate;
@end
