//
//  RestApi+getAllSubscriptions.m
//  ICommConnect
//
//  Created by Ravikanth on 12/2/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "RestApi+getAllSubscriptions.h"

@implementation RestApi (getAllSubscriptions)
- (void) getAllSubscriptions:(id <GetSubscriprionsDelegate,RestDelegate>) delegate{
    NSString *endpoint = [NSString stringWithFormat:@"/ICommClient/GetAllActiveSubscriptions"];
    [self executeRequest:endpoint delegate:delegate success:@selector(getAllSubscriptionsSuccess:) failure:nil];
}
/*Success CallBack*/
- (void) getAllSubscriptionsSuccess:(RestApi*)object{
    id<GetSubscriprionsDelegate> delegatee=object.delegated;
    NSArray *response = (NSArray*)[self responseDictionary];
    [delegatee handlegetSubscriptionsResult:response];
}

@end
