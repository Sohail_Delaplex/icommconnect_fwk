//
//  RestApi.h
//  TestApp
//
//  Created by Ravikanth on 9/13/15.
//  Copyright (c) 2015 Ravikanth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestDelegate.h"
@interface RestApi : NSObject<RestDelegate>
@property (assign) int statusCode;
@property (assign) SEL finishSelector;
@property (assign) SEL failSelector;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSMutableData *responseData;
@property (assign) id delegated;
- (void) executeRequest:(NSString *) endpoint delegate:(id<RestDelegate>) delegate success:(SEL)success failure:(SEL)failure;
-(NSData*)synchronousRequestUrl:(NSString*)urlString;
- (void) setDidFinishSelector:(SEL) selector;
- (void) setDidFailSelector:(SEL) selector;
- (NSString *) responseString;
- (NSMutableDictionary *) responseDictionary;
@end
