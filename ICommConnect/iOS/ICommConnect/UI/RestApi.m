//
//  RestApi.m
//  TestApp
//
//  Created by Ravikanth on 9/13/15.
//  Copyright (c) 2015 Ravikanth. All rights reserved.
//

#import "RestApi.h"
#import "Constants.h"
@implementation RestApi
@synthesize finishSelector;
@synthesize failSelector;
@synthesize statusCode;
@synthesize responseData;
@synthesize delegated;
@synthesize error;
/*Executing a URL Request and Establishing URL Connection with CallBacks*/
- (void) executeRequest:(NSString *) endpoint delegate:(id<RestDelegate>) delegate success:(SEL)success failure:(SEL)failure{
    NSString *urlString=[NSString stringWithFormat: @"%@%@",kBaseURL,endpoint];
    NSLog(@"urlString:%@",urlString);
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 20.0];
[NSURLConnection connectionWithRequest:urlRequest delegate:self];
   responseData = [NSMutableData data];
    if ( delegate!= nil) {
        [self setDelegated:delegate];
    }
    if (failure != nil) {
        [self setDidFailSelector:failure];
    }else {
        [self setDidFailSelector:@selector(operationFailed)];
    }
    if (success != nil) {
        [self setDidFinishSelector:success];
    }

}
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
   
    if ([response isMemberOfClass:[NSHTTPURLResponse class]]) {
        NSHTTPURLResponse *resp = (NSHTTPURLResponse *) response;
        self.statusCode = (int)[resp statusCode];
    }
    [responseData setLength:0];
}
-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
}
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)receivedError
{
     self.error=receivedError;
    NSLog(@"receivedError:%@",receivedError);
       [self performSelector:self.failSelector withObject:self];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)conn
{
    if(statusCode == 200) {
        [[NSUserDefaults standardUserDefaults] setObject:@"success" forKey:@"Connection"];
        [self performSelector:self.finishSelector withObject:self];
    } else {
        [self performSelector:self.failSelector withObject:self];
    }
}
-(NSData*)synchronousRequestUrl:(NSString*)urlString{
    
    urlString = [[NSString stringWithFormat:@"%@%@",kBaseURL,urlString]
                 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"urlString:%@",urlString);
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString ] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
    NSURLResponse * response = nil;
    NSError * errorres = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:&errorres];
    if (errorres != nil)
    {
        // NSLog(@"response:%@",error);
    }
    return data;
}

/*Setting Failure Selector*/

- (void) setDidFailSelector:(SEL) thefailSelector {
    self.failSelector = thefailSelector;
}
/*Setting Success Selector*/
- (void) setDidFinishSelector:(SEL)thefinishSelector {
    self.finishSelector = thefinishSelector;
}
- (void)operationFailed{
    id <RestDelegate> delegate = self.delegated;
    [delegate handleError:error withStatus:statusCode];
}

#pragma clang diagnostic pop
- (NSMutableDictionary *) responseDictionary {
    NSMutableDictionary *respDictionary = [NSJSONSerialization
                                           JSONObjectWithData:self.responseData
                                           options:NSJSONReadingMutableContainers
                                           error:nil];
    return respDictionary;
}
- (NSString *) responseString {
    return [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
}

@end
