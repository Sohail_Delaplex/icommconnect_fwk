//
//  RestDelegate.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RestDelegate
@optional
- (void) handleError:(NSError *) error withStatus:(int) statusCode;

@end