//
//  SipDelegate.h
//  ICommConnect
//
//  Created by Ravikanth on 9/14/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SipDelegate <NSObject>
- (void) handleSIPResult:(NSMutableDictionary*)responseDictionary;
@end
