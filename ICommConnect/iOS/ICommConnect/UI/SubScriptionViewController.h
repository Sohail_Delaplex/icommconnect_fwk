//
//  Created by Ravikanth on 8/19/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "BaseViewController.h"
#import "SubscriberInfoDelegate.h"
#import "DepartmentsDelegate.h"
#import "SeacrhDelegate.h"
@interface SubScriptionViewController : BaseViewController<SubscriberInfoDelegate,DepartmentsDelegate,SeacrhDelegate>
@property (retain, nonatomic) IBOutlet UIImageView *logoImageView;
@property (retain, nonatomic) IBOutlet UILabel *defaultContactTitleLabel;
@property (retain, nonatomic) IBOutlet UITextField *searchTextField;
@property (retain, nonatomic) IBOutlet UIButton *defaultContactButton;
@property (retain, nonatomic) IBOutlet UITableView *departmentsTableView;
@property (retain, nonatomic) IBOutlet UIButton *homeButton;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;
@property (retain, nonatomic) IBOutlet UIView *searchView;
@property (retain, nonatomic) IBOutlet UIView *topHeaderView;
@property (retain, nonatomic) IBOutlet UITableView *searchResultTable;
@property (assign) BOOL isSearchEnabled;
- (IBAction)buttonAction:(id)sender;
-(void)refreshSettings;
- (IBAction)homeButtonAction:(id)sender;
-(void)getSubscriberInfo;
@end
