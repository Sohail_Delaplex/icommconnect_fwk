//
//  Created by Ravikanth on 8/19/15.
//  Copyright (c) 2015 Doubango Telecom. All rights reserved.
//

#import "SubScriptionViewController.h"
#import "ICommConnectAppDelegate.h"
#import "DepartmentViewController.h"
#import "DepartmentTableViewCell.h"
#import "AudioCallViewController.h"
#import "ContactViewController.h"
#import "RestApi+SubsriberInfo.h"
#import "RestApi+Departments.h"
#import "RestApi+Search.h"
@interface SubScriptionViewController (){
    float cellHeight;
    NSMutableDictionary *responseData;
    NSMutableDictionary *subscriptionInfoDictionary;
    NSMutableDictionary *sortedDictionary;
    NSMutableArray *departmentNames;
    NSArray *searchResultsArray;
    NSData *logoImageData;
    UIView *view;
    NSMutableArray *cellHieghtsArray;
}
@end
@implementation SubScriptionViewController
@synthesize searchResultTable;
@synthesize isSearchEnabled;
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshApp"]) {
        self.homeButton.hidden=YES;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RefreshApp"];
        //clearing this result to show back button at detail contact
    }
    self.subscription_ID=[self.standardUserDefaults objectForKey:@"SubscriptionGUId"];
    self.searchResultTable.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}
//app refreshing whenever there is no connection

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    if([self getObjectForKey:@"Connection"]){
        if ([[self getObjectForKey:@"Connection"] isEqualToString:@"Failed"]) {
            [self viewWillAppear:YES];
        }
    }
    
}

-(void)autoResizeTheView{
    float viewHeight;
    if (!isSearchEnabled) {
        self.searchView.hidden=YES;
        viewHeight=155;
    }else{
        self.searchView.hidden=NO;
        viewHeight=199;
    }
    [self.topHeaderView removeConstraint:_topViewHeightConstraint];
    _topViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.topHeaderView                                                             attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:viewHeight];
    [self.topHeaderView addConstraint:_topViewHeightConstraint];
}
- (void)viewWillAppear:(BOOL)animated{
    [self clearEmptycells];
    cellHieghtsArray=[[NSMutableArray alloc ] init];
    [self autoResizeTheView];
    [self.defaultContactTitleLabel setFont:subTitleFont];
    [self getSubscriberInfo];//getting Subscriber info
    [self _prepareTable];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

-(void)refreshSettings{
    [self viewWillAppear:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numberOfRows;
    if (tableView==searchResultTable) {
        numberOfRows=(int)[searchResultsArray count];
    }else{
        numberOfRows=(int)[departmentNames count];
    }
    return numberOfRows;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float tabelviewCellHeight;
    if (tableView==searchResultTable) {
        tabelviewCellHeight=30;
    }else{
        if (cellHieghtsArray.count>0)
        tabelviewCellHeight=[[cellHieghtsArray objectAtIndex:indexPath.row] integerValue];else
            tabelviewCellHeight=([[sortedDictionary objectForKey:[departmentNames objectAtIndex:indexPath.row]] count]/3+1)*90+30;
    }
    return tabelviewCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCellStyle style =  UITableViewCellStyleSubtitle;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    cell = [[UITableViewCell alloc] initWithStyle:style reuseIdentifier:@"BaseCell"];
    if (tableView==searchResultTable) {
        cell.textLabel.text=[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
    }else{
        //loading departments of a subscriber
        view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
        cellHeight=[self designCustomView:view withDepartment:[sortedDictionary objectForKey:[departmentNames objectAtIndex:indexPath.row]] title:[departmentNames objectAtIndex:indexPath.row]];
        [cellHieghtsArray addObject:[NSString stringWithFormat:@"%f",cellHeight]];
        cell.backgroundColor=[UIColor clearColor];
        [cell.contentView addSubview:view];
    }
    
    return cell;
}

- (void)orientationChanged:(NSNotification *)notification{
    UIInterfaceOrientation orientation = (UIInterfaceOrientation)[[UIDevice currentDevice] orientation];
    if(orientation == UIInterfaceOrientationPortrait||UIInterfaceOrientationLandscapeRight||UIInterfaceOrientationLandscapeLeft||UIInterfaceOrientationPortraitUpsideDown)
    {
        [cellHieghtsArray removeAllObjects];
        [self.departmentsTableView reloadData];
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"IsDepartment"] boolValue]){
        /* If selected item is Department type from the search results need to load that department */
        DepartmentViewController *pushToController = [self.storyboard instantiateViewControllerWithIdentifier:@"DepartmentView"];
        pushToController.departmentID=[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"GUId"];
        [self.navigationController pushViewController:pushToController animated:YES];
        
    }else{
         /* If selected item is Contact type from the search results need to load that contact data */
        ContactViewController *pushToController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactView"];
        pushToController.contactGuid=[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"GUId"];
        [self.navigationController pushViewController:pushToController animated:YES];
    }
    [self clearSearchData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [self clearSearchData];
}
- (void)dealloc {
    responseData=nil;
    subscriptionInfoDictionary=nil;
    sortedDictionary=nil;
    departmentNames=nil;
    searchResultsArray=nil;
    logoImageData=nil;
    cellHieghtsArray=nil;
}
/* invoking subscriber information api*/
-(void)getSubscriberInfo{
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    [[RestApi alloc] getSubScriberInfo:self forSubscriberId:[self.standardUserDefaults objectForKey:@"SubscriptionGUId"]];
}
/* invoking subscriber departments api*/
-(void)getDepartments{
     [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    [[RestApi alloc] getSubScriberDepartments:self forSubscriberId:[self.standardUserDefaults objectForKey:@"SubscriptionGUId"]];
}
/*Here subscriber info api data will be handled */
-(void)handleSubscriberInfoResult:(NSMutableDictionary *)responseDictionary{
    if(![responseDictionary isKindOfClass:[NSNull class]]&&[responseDictionary count]>0)
    {
        isSearchEnabled=[[responseDictionary objectForKey:@"ShowSearch"] boolValue];
        [self autoResizeTheView];
        subscriptionInfoDictionary=responseDictionary;
        self.logoImageView.hidden=NO;
        self.defaultContactButton.hidden=NO;
        self.defaultContactTitleLabel.text=[NSString stringWithFormat:@"%@",[responseDictionary valueForKeyPath:@"DefaultContactLabel"]];
        [self setLocalObject:[responseDictionary objectForKey:@"DirectoryiconColor"] forKey:@"DirectoryiconColor"];
        [self setLocalObject:[responseDictionary valueForKeyPath:@"DirectoryBackgroundColor"] forKey:@"DirectoryBackgroundColor"];
       NSString *str= [NSString stringWithFormat:@"%@",[responseDictionary valueForKeyPath:@"SubID"]];
        
         [self setLocalObject:[NSString stringWithFormat:@"%@",[responseDictionary valueForKeyPath:@"SubID"]] forKey:@"SubID"];
        [self loadViewbackgroundColor];
        [self setNavigationBarSettings];
        //setting logo
        if (![[responseDictionary valueForKeyPath:@"Logo"] isKindOfClass:[NSNull class]]){
            [self loadImageWithUrl:[responseDictionary valueForKeyPath:@"Logo"] andImageView:self.logoImageView];
        }//checking subscription start date and expiration
        if (![[responseDictionary objectForKey:@"IsStarted"] boolValue] || [[responseDictionary objectForKey:@"IsExpired"] boolValue] || [[responseDictionary valueForKeyPath:@"Status"] isEqualToString:@"Delinquent"]) {
            [SVProgressHUD dismiss];
            self.defaultContactTitleLabel.hidden=YES;
             self.defaultContactButton.hidden=YES;
            self.searchView.hidden=YES;
            [self showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"%@%@",Delinquent_Message,[responseDictionary valueForKeyPath:@"DefaultContact"]]];
            return;
        }//saving Tile background image
        if(![[responseDictionary objectForKey:@"DirectoryIconPath"] isKindOfClass:[NSNull class]]){
            NSData *imageData= [NSData dataWithContentsOfURL:[NSURL URLWithString:[responseDictionary valueForKeyPath:@"DirectoryIconPath"]]];
            [self setLocalObject:imageData forKey:@"DirectoryIcon"];
        }
        self.logoImageView.contentMode=UIViewContentModeScaleAspectFit;
    }
    [SVProgressHUD dismiss];
    [self getDepartments];
}
//here adding a new key ObjecID for each department, this id will be given as tag to button to identify which department was selected.
-(id)addidToObjects:(NSMutableDictionary*)responseDictionary{
    NSMutableArray *modernArray=[[NSMutableArray alloc] init];
    int i=1;
    for (NSMutableDictionary *obj in responseDictionary) {
         NSMutableDictionary *objectDict=[[NSMutableDictionary alloc] initWithDictionary:obj];
        [objectDict setObject:[NSString stringWithFormat:@"%d",i] forKey:@"ObjectID"];
        i++;
        [modernArray addObject:objectDict];
    }
    NSMutableDictionary *dict=(NSMutableDictionary*)modernArray;
   // NSLog(@"dict:%@",dict);
    return modernArray;
}
/* handling departments api results  data*/
- (void) handleDepartmentsResult:(NSMutableDictionary*)responseDictionary{
    
   // NSLog(@"Count:%lu",(unsigned long)responseDictionary.count);
    responseData=[self addidToObjects:responseDictionary];
    sortedDictionary = [self groupTheDepartmentsByGroupName:responseData];
    NSArray *keys = [sortedDictionary allKeys];
    departmentNames = [keys mutableCopy];
    departmentNames=[self sortTheArray:departmentNames];
    [self.departmentsTableView reloadData];
    [self.departmentsTableView setContentOffset:CGPointZero animated:YES];
    [SVProgressHUD dismiss];//dismissing progress bar
}
/* handling serach results API data*/
-(void)handleSearchResult:(NSData *)receivedData{
    searchResultsArray=[NSJSONSerialization JSONObjectWithData:receivedData options:0 error:nil];
    [searchResultTable reloadData];
    searchResultTable.hidden=NO;
    searchResultTable.frame = CGRectMake(searchResultTable.frame.origin.x, searchResultTable.frame.origin.y, searchResultTable.frame.size.width, searchResultTable.contentSize.height);
}
/* loading image for a given URL on backround*/
-(void)loadImageWithUrl:(NSString*)url andImageView:(UIImageView*)imageView{
    dispatch_queue_t DownloadQueue = dispatch_queue_create("Download Pic", NULL);
    dispatch_async(DownloadQueue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (imageView==self.logoImageView) {
                @try {
                    imageView.image=[UIImage imageWithData:imageData];
                    logoImageData=imageData;
                    [self setLocalObject:imageData forKey:@"logoImageData"];
                }
                @catch (NSException *exception) {
                    imageView.image=[UIImage imageNamed:@"Logo_default.png"];
                }


            }
        });
    });
}
//home button Action
- (IBAction)homeButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
//Making a call to the default contact number
- (IBAction)buttonAction:(id)sender {
    UIButton *btn=(UIButton*)sender;
    [self setLocalObject:NULL forKey:@"contactGuid"];
    AudioCallViewController *presentController = [self.storyboard instantiateViewControllerWithIdentifier:@"AudioView"];
    presentController.contactDictionary=subscriptionInfoDictionary;
    presentController.imageData=logoImageData;
    presentController.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:presentController animated:YES completion:nil];
}
//This method will be invoke when user taps on a tile
-(void)btnAction:(UIButton *)sender{
    UIButton *btn=(UIButton*)sender;
  //  NSLog(@"tag:%ld",sender.tag);
     NSString *selectedDepartmentGuid;
    for (id obj in responseData) {
        NSString *keyValue = [obj valueForKey:@"ObjectID"];
        if([keyValue integerValue]==btn.tag){
            DepartmentViewController *pushToController = [self.storyboard instantiateViewControllerWithIdentifier:@"DepartmentView"];
            pushToController.departmentID=[obj objectForKey:@"Guid"];
            [self.navigationController pushViewController:pushToController animated:YES];
            return;
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSCharacterSet* numberCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"];
    for (int i = 0; i < [string length]; ++i)
    {
        unichar c = [string characterAtIndex:i];
        if (![numberCharSet characterIsMember:c])
        {
            return NO;
        }else{
            if (textField.text.length>0) {
                [[RestApi alloc] getSearchResults:self withTerm:newString withSubscriptionGuid:[self.standardUserDefaults objectForKey:@"SubscriptionGUId"]];          //calling search results api
            }

        }
    }
    return !([newString length] == 20);
    
    return YES;
}
/*  clear search data*/
-(void)clearSearchData{
    self.searchResultTable.hidden=YES;
    self.searchTextField.text=@"";
    [self.searchTextField resignFirstResponder];
}
/* adding touch events on table view to clear the search data when tapped on table view*/
-(void)_prepareTable {
    UITapGestureRecognizer *anyTouch =
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(tableTap)];
    [self.departmentsTableView addGestureRecognizer:anyTouch];
}

/*clear the search data and ending editing textfields in a view when tapped on view */
- (void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *)event {
    [self clearSearchData];
    [self.view endEditing:YES];
}
-(void)tableTap {
[self clearSearchData];
}
- (void)clearEmptycells
{
searchResultTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
@end
