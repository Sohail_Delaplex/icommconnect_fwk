//
//  UIColor+StringtoColor.h
//  ScreenButtons
//
//  Created by Jaya Sheela Poranki on 19/08/15.
//  Copyright (c) 2015 Prime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (StringtoColor)
+(UIColor*) colorFromHexString:(NSString*) hexString;
@end
